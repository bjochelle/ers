	<div id="modalViewScore" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> View Score </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
				<div class="col-md-12 input-group">
					<input style="border: 1px solid #ffffff;background: white;" disabled type="hidden" class="form-control" name="id" required="" id="view_id">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Quiz Name :  </span>
                    </div>

                     <span id="quiz_name"> </span>
                </div> 
				        <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;">Subject :  </span>
                    </div>

                       <span id="subject"> </span>
                </div> 
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;">Level :  </span>
                    </div>

                        <span id="level"> </span>
                </div> 
				       
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Teacher :  </span>
                    </div>
                     <span id="teacher"> </span>
                </div>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Date Added :  </span>
                    </div>
                      <span id="date_added"> </span>
                </div>
				</div>
			</div>
		</div>
	</div>

  <style type="text/css">
    span{
          padding: 6px;
    }
  </style>