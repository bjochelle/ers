<form action="" method="POST" id="frm_updateFaculty">
	<div id="modal_updateFaculty" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
                        <h4 class="modal-title"> Update Faculty</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code : <span style="color:red;"> *</span></span>
                             </div>
                            <input type="text" class="form-control" id="up_code" name="u_code" placeholder="Full Name" autocomplete="off">
                        </div>

               			 <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Name:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<input type="text" class="form-control" id="up_name" name="u_name" placeholder="Full Name" autocomplete="off">
            		    </div>
            		     <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Year:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<!-- <input type="text" class="form-control" id="year" name="year" placeholder="Year" autocomplete="off"> -->
                 		 	<select class="form-control" id="up_year" name="u_year"required>
                 		 		<option value=""> --Select-- </option>
                 		 		<option value="1"> 1st Year </option>
                 		 		<option value="2"> 2nd Year </option>
                 		 		<option value="3"> 3rd Year </option>
                 		 		<option value="4"> 4th Year </option>
                 		 	</select>
            		    </div>
            		     <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Section:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<input type="text" class="form-control" id="up_section" name="u_section" placeholder="Section" autocomplete="off">
            		    </div>
            		</div>
            		<input type="hidden" id="hidden_id" name='user_id'>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_updateFaculty" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Update</button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>