<!-- <form action="" method="POST" id="form_vis_login"> -->
	<div id="modal_vis_login" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="  background: #f70d60;
    color: white;">
                        <h4 class="modal-title"> Visitor's Login </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Name : <span style="color:red;"> *</span></span>
                             </div>
                            <input type="text" class="form-control" id="vis_name" name="vis_name" placeholder="Full Name" autocomplete="off" style="width: 75%;border: 1px solid #a5adb5;box-shadow: none;">
                        </div>

               			 <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Purpose:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<input type="text" class="form-control" id="purpose" name="purpose" placeholder="Purpose" autocomplete="off" style="width: 75%;border: 1px solid #a5adb5;box-shadow: none;">
            		    </div>
            		</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="vis_login_submit" class="btn btn-sm btn-primary"><span class="fa fa-sign-in"></span> Login</button>
					</span>
				</div>
			</div>
		</div>
	</div>
<!-- </form> -->