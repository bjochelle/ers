<form action="" method="POST" id="add_faculty">
    <div id="modalAddFaculty" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <h4 class="modal-title"> Add Teacher</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                        <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">First Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="fname" required="" placeholder="First Name">
                   </div> <br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Last Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="lname" required="" placeholder="Last Name">
                   </div> <br>


                            <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Grade : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="number" class="form-control" name="year" required="" placeholder="Grade">
                </div> <br>



                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Section : <span style="color:red;">*</span> </span>
                    </div>

                    <input type="number" class="form-control" name="section" required="" placeholder="Section">
                </div>
                </div>
                <div class="modal-footer input-group-btn">
                    <span class="btn-group" role="group">
                        <button type="submit" id="btn_add" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save </button>
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</form>