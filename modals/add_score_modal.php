<form action="" method="POST" id="add_score">
	<div id="addScore" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
                    <h4 class="modal-title"> Add Score (Quiz) </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">
                        <input type="hidden" id="audioID" name="audioID">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Score:<span style="color:red;"> *</span></span>
                             </div>
                            <input type="number" class="form-control" name="score" placeholder="Quiz Score" autocomplete="off">
                        </div>

               		
            		     
            		</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_add" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save</button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>