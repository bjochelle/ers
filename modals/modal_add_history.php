<form action="" method="POST" id="add_history">
	<div id="modalAddHistory" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
                    <h4 class="modal-title"> Add Advisory </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">
                        <input type="hidden" class="form-control" id="add_history_t_id" name="t_id">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Year:<span style="color:red;"> *</span></span>
                             </div>
                            <input type="number" class="form-control" name="year" placeholder="Year" autocomplete="off">
                        </div>

               			 <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Section:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<input type="number" class="form-control" name="section" placeholder="Section" autocomplete="off">
            		    </div>
            		     
            		</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_add" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save</button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>