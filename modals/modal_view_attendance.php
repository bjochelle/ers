	<div id="modalViewAttendance" class="modal fade" role="dialog">
		<div class="modal-dialog"  style="max-width: 55% !important;">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> View Score </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
        <div class="col-12">
              <div class="card mt-3 tab-card">
                <div class="card-header tab-card-header">
                  <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Table View</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Graphical View</a>
                    </li>
                  </ul>
                </div>

                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                  <table id="view_attendance" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>

                      <th>#</th>
                      <th>Subject</th>
                      <th>Category</th>
                      <th>Score</th>
                      <th>Date Added</th>
                    </tr>
                    </thead>
                    <tbody>
                  
                  
                    </tbody>
                
                  </table>
                              
                  </div>
                  <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                   <div id='chart'></div>              
                  </div>

                </div>
              </div>
            </div>
				</div>
			</div>
		</div>
	</div>