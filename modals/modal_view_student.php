	<div id="modalViewStudent" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> View Student Information </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
				<div class="col-md-12 input-group">
					<input style="border: 1px solid #ffffff;background: white;" disabled type="hidden" class="form-control" name="id" required="" id="view_id">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Student Id :  </span>
                    </div>

                     <input style="border: 1px solid #ffffff;background: white;" disabled type="text" class="form-control" name="stud_id" required="" placeholder="Student Id" id="view_stud_id">
                </div> 
				<div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;">First Name :  </span>
                    </div>

                     <input style="border: 1px solid #ffffff;background: white;" disabled type="text" class="form-control" name="fname" required="" placeholder="First Name" id="view_fname">
                </div> 
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;">Last Name :  </span>
                    </div>

                     <input style="border: 1px solid #ffffff;background: white;" disabled type="text" class="form-control" name="lname" required="" placeholder="Last Name" id="view_lname">
                </div> 
				<div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;">Year :  </span>
                    </div>
                     <input style="border: 1px solid #ffffff;background: white;" disabled type="number" class="form-control" name="year" required="" placeholder="Year" id="view_year">
 
                </div> 

                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Section :  </span>
                    </div>

                    <input style="border: 1px solid #ffffff;background: white;" disabled type="number" class="form-control" name="section" required="" placeholder="Section" id="view_section">
                </div>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Teacher Advisory :  </span>
                    </div>

                    <input style="border: 1px solid #ffffff;background: white;" disabled type="text" class="form-control" name="section" required="" placeholder="Advisory" id="view_teacher">
                </div>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Email :  </span>
                    </div>

                    <input style="border: 1px solid #ffffff;background: white;" disabled type="email" class="form-control" name="section" required="" id="view_email">
                </div>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Address :  </span>
                    </div>

                    <input style="border: 1px solid #ffffff;background: white;" disabled type="text" class="form-control" name="section" required="" id="view_add">
                </div>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="background-color: #ffffff !important;border: 1px solid #ffffff !important;font-weight: bold;"> Contact No. :  </span>
                    </div>

                    <input style="border: 1px solid #ffffff;background: white;" disabled type="number" class="form-control" name="section" required="" id="view_contact">
                </div>
				</div>
			</div>
		</div>
	</div>