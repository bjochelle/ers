<form action="" method="POST" id="upload_student">
	<div id="modalStudent" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><span class="fa fa-cloud-upload"></span> Upload Student Entry (csv)</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><strong>Upload file (csv): <span style="color:red;">*</span></strong></span>
						<input type="file" class="form-control" aria-describedby="basic-addon1" name="student" required/>
					</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_upload" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Upload Entries</button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>