<form action="" method="POST" id="update_subject">
	<div id="modalUpdateStudent" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> Update Subject</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
				<div class="col-md-12 input-group">
					<input type="hidden" class="form-control" name="subject_id" required="" id="subject_id">
                <div class="col-md-12 input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Subject Code : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="subject_code" id="subject_code" required="" placeholder="Subject Code">
                   </div> <br><br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Subject Description : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="subject_name" id="subject_name" required="" placeholder="Subject Description">
                   </div> 
                 </div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_update" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Update </button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>