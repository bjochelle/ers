<?php 
  session_start();
  include 'core/config.php';
  $id= $_SESSION['id'];
   $name= $_SESSION['name'];
  $user_type= $_SESSION['user_type'];

  $sql = ($user_type == 'S')? "SELECT * from tbl_student where stud_id = '$id'" : "SELECT * from tbl_user where user_id = '$id'";

  $query = $connectDB->query($sql);
  $data = mysqli_fetch_array($query);

if($id == ""){
  header("Location: index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MANLUCAHOC ELEMENTARY SCHOOL</title>

<!--   <link rel="icon" type="image/ico" href="images/images.jpg" /> -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="dist/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
 
   <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">

<link rel="stylesheet" href="assets/css/animate.css">

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="dist/js/bootstrap-notify.js"></script>
<script src="dist/js/bootstrap-notify.min.js"></script>

  <!-- bootstrap time picker -->
<script src="dist/js/date-time-picker.min.js"></script>



  <script type="text/javascript">
      $(document).ready(function(){


  $('[data-toggle="tooltip"]').tooltip();
  $('#datepicker').dateTimePicker();
  $('#datepicker2').dateTimePicker();
  $('#datepicker3').dateTimePicker();

  var current_date = "<?php echo date("Y-m-d"); ?>";
  var min = new Date(current_date);
  var max = new Date(current_date);
  var mindate = min.setDate(min.getDate());
  var maxdate = max.setDate(max.getDate());
  $('#datepicker_min').dateTimePicker({
    mode:'date',
    limitMin: mindate
  });
  
  $('#datepicker_min1').dateTimePicker({
    mode:'date',
    limitMax: maxdate
  });


});



function checkAll(ele) {
      var checkboxes = document.getElementsByTagName('input');
      if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = true;
            }
          }
      } else {
        for (var i = 0; i < checkboxes.length; i++) {
          //console.log(i)
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = false;
          }
        }
      }
    }

function success_add(){
  $.notify("<strong><span class='fa fa-check-circle'></span> All Good! </strong> Record was successfully added.");
}
function success_delete(){
  $.notify("<strong><span class='fa fa-check-circle'></span> All Done! </strong> Selected records were deleted.");
}
function success_update(){
  $.notify("<strong><span class='fa fa-check-circle'></span> Success! </strong> Record was updated.");
}



//dynamic alert
function alertMe(title, message, type){
  $.notify({
    title: '<strong>'+title+'</strong>',
    message: message
  },
  {
    type: type
  });
}

function failed_query(){
  $.notify({
    title: '<strong> Something is wrong!</strong>',
    message: 'Failed to execute query.'
  },
  {
    type: 'danger'
  });
}

function exist(){
  $.notify({
    title: '<strong> Existing Student Id / Name </strong>',
    message: 'Please double check.'
  },
  {
    type: 'danger'
  });
}

function no_teacher(){
  $.notify({
    title: '<strong> Error </strong>',
    message: 'No teacher Found.'
  },
  {
    type: 'danger'
  });
}


function exist_year(){
  $.notify({
    title: '<strong> Error. </strong>',
    message: 'Existing Year and Section'
  },
  {
    type: 'danger'
  });
}
</script>
  <style type="text/css">
    .sidebar-dark-primary .sidebar a{
      color: #09509c;
    }
    .sidebar-dark-primary .sidebar a:hover{
      background: #09509c !important;
      color:white !important;
    }
    [class*=sidebar-dark] .user-panel{
        border-bottom: 2px solid #0f7b0a ;
    }
    .modal-header{
      background: #09509c;
          color: white;
    }

    .close{
      color:white;
    }
    .dataTables_wrapper {
    	overflow-x: scroll;	
    }
  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" style="background: #09509c !important;">
    <!-- Left navbar links -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
      <li >
        <a class="nav-link" data-widget="pushmenu" href="dashboard.php"><i class="fa fa-bars" style="color: white;"></i></a>
      </li>

       <li>
        <a class="nav-link"  style="color:white; font-weight: bold;" href="#" >
            MANLUCAHOC ELEMENTARY SCHOOL
        </a>
      </li>
    </ul>

    </div>
    <ul class="navbar-nav ml-auto">

      <li class="nav-item dropdown ">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
          <i class="fa fa-user" style="color: #ffffff !important;"></i> <span class="brand-text font-weight-light" style="color: white;
        font-size: 20px;">  <?php echo $data['fname']." ".$data['lname'];?>   <span class="fa fa-caret-down"> </span>   
        </span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right ">
          <span class="dropdown-item dropdown-header">Settings</span>
          <div class="dropdown-divider"></div>
          <a href="profile.php" class="dropdown-item">
            <i class="fa fa-user mr-2"></i>  Profile
          </a>
          <div class="dropdown-divider"></div>
          <a href="ajax/logout.php" class="dropdown-item">
            <i class="fa fa-sign-out mr-2"></i> Logout
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: white">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link" style="background: #09509c;">
   <!--    <img src="images/images.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <span class="brand-text font-weight-bold" style="color:#ffffff;">ERS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="background: #fff;">
      <!-- Sidebar user panel (optional) -->
   <!--    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="dashboard.php" class="d-block"><i class="nav-icon fa fa-dashboard"></i> Dashboard</a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fa fa-bullhorn"></i>
              <p>
              Announcement
              </p>
            </a>
          </li>

              <?php 
                if($user_type !== "S"){ ?>
          <li class="nav-item">
            <a href="modules.php" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Phil - IRI
              </p>
            </a>
          </li>

         <li class="nav-item">
            <a href="quiz.php" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                  READING EXERCISE
              </p>
            </a>
          </li>
           
          <li class="nav-item">
            <a href="student.php" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Students
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="subject.php" class="nav-link">
              <i class="nav-icon fa fa-list"></i>
              <p>
                Subject 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="schedule.php" class="nav-link">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                Schedule Announcement
              </p>
            </a>
          </li>

           <?php if($user_type === "A"){ ?>
          <li class="nav-item">
            <a href="maintenance.php" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>Teachers</p>
            </a>
          </li>
           <?php } ?>
         <!--  <li class="nav-item">
            <a href="attendance.php" class="nav-link">
              <i class="nav-icon fa fa-tasks"></i>
              <p>Attendance</p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="reports.php" class="nav-link">
              <i class="nav-icon fa fa-bar-chart"></i>
              <p>Reports</p>
            </a>
          </li>
            <?php }else{?>


            <li class="nav-item">
            <a href="modules.php" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                  Phil - IRI
              </p>
            </a>
          </li>

         <li class="nav-item">
            <a href="quiz.php" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                  READING EXERCISE
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="score.php" class="nav-link">
              <i class="nav-icon fa fa-star"></i>
              <p>
               Scores
              </p>
            </a>
          </li>



              <?php } ?>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>