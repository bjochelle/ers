

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MANLUCAHOC ELEMENTARY SCHOOLss</title>
    <!-- 	<link rel="icon" type="image/ico" href="images/images.jpg" /> -->
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->

    <link href="login_asset/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>

    <!-- banner slider css file -->
    <link href="login_asset/css/JiSlider.css" rel="stylesheet">
    <!-- //banner slider css file -->

    <!-- css files -->
    <link rel="stylesheet" href="login_asset/css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="login_asset/css/style.css" type="text/css" media="all" /> <!-- Style-CSS -->
    <link rel="stylesheet" href="login_asset/css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- //web-fonts -->
    <style type="text/css">
        .pb-5, .py-5{
            padding-bottom: 2rem !important;
            padding-top: 2rem !important;
        }
        @media (min-width: 992px){
            .navbar-expand-lg .navbar-collapse {
                display: -webkit-box !important;
                display: -ms-flexbox !important;
                /* display: flex !important; */
                -ms-flex-preferred-size: auto;
                flex-basis: auto;
            }
        }
        .resp-tab-item h3{
            background: #0f7b0a;
            border-color: #0f7b0a;
        }
        .map iframe {
            border: 3px solid #274520 !important;
        }
    </style>

</head>

<body>
<!-- header -->
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <!-- 	<img src="images/images.jpg" style="width:7%;" />  --><span style="color:#09509c">MANLUCAHOC ELEMENTARY SCHOOL</span>
            </a>
            <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


        </nav>
    </div>
</header>
<!-- //header -->
<!-- About -->
<section class="about py-5">
    <div class="container-fluid py-lg-5 py-3">
        <div class="row about_grids">
            <div class="col-lg-4 col-md-6 team_grid1">
                <h3 class="heading text-uppercase">About Us</h3>
                <ul>
                    <li>School ID : 117377</li>
                    <li>School Name : Manlucahoc Elementary School </li>
                    <li>School Address : Manlucahoc Elementary School, Sipalay City, Negros Occidental </li>
                    <li>Municipality : SIPALAY CITY</li>
                    <li>Region : NIR</li>
                    <li>Province : Negros Occidental </li>
                    <li>Division : Negros Occidental </li>
                    <li>Legistative District : 6th District </li>
                    <li>Curricular Class : Kinder & Elementary</li>
                    <li>Date of Operation : Wednesday, January 01, 1964</li>
                    <li>District : Sipalay II</li>
                    <li>Classification : Local Government</li>
                    <li>School Type : School with no Annexes</li>
                    <li>Class Organization : Monograde</li>
                </ul>
            </div>

            <div class="col-lg-5 col-md-6 mt-md-0 mt-5 map">
                <iframe style="height: 500px;width: 100%;" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&q=9.7200833,122.4896783&maptype=satellite&zoom=16" allowfullscreen></iframe>
            </div>
            <div class="col-lg-3 col-md-6 about_img mt-md-0 mt-5 " style="top: 100px;">
                <form>
                    <h3 class="heading text-uppercase">Login </h3>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Student No.: </label>
                        <input type="number" class="form-control"  id="student_id" aria-describedby="emailHelp" name="student_id" placeholder="Identification number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Password.: </label>
                        <input type="text" class="form-control"  id="pw" aria-describedby="emailHelp" name="pw" placeholder="Password">
                    </div>

                    <button type="button" id="login" class="btn btn-info btn-lg-block w3ls-btn px-4 text-uppercase mr-2" aria-pressed="false" style="background: #09509c;border-color: #09509c;">Login</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- //About -->
<div class="row" style="border: 3px solid #09509c;"></div>


<!-- Team -->
<section class="team py-5 my-lg-5 my-3">
    <div class="container-fluid ">
        <div class="row team_grids">
            <div class="col-lg-12 col-12 mb-lg-0 mb-4 team_grid1 text-center">
                <h3 class="heading text-uppercase">Our Team</h3>
                <p>Our team is from Carlos Hilado Memorial State College - Alijis Campus.</p>
                <p class="second_para"> This Website is partial fulfillment of the requirements for the degree of Bachelor of Science in Information System.</p>
            </div>
            <div class="col-lg-4 col-md-3 col-6 mb-md-0 mb-5 w3l_team_grid">
                <div class="view view-second">
                    <img src="images/test3.jpg" alt=" " class="img-fluid" />
                </div>
                <h4 class="my-2" style="color: #09509c;">xxxxxxx</h4>
                <p>Project Manager</p>
            </div>
            <div class="col-lg-4 col-md-3 col-6 mb-md-0 mb-5 w3l_team_grid">
                <div class="view view-second">
                    <img src="images/test3.jpg" alt=" " class="img-fluid" />
                </div>
                <h4 class="my-2" style="color: #09509c;">xxxxxxx</h4>
                <p>Programmer</p>
            </div>
            <div class="col-lg-4 col-md-3 col-6 w3l_team_grid">
                <div class="view view-second">
                    <img src="images/test3.jpg" alt=" " class="img-fluid" />
                </div>
                <h4 class="my-2" style="color: #09509c;">xxxxxxx</h4>
                <p>System Analyst</p>
            </div>
        </div>
    </div>
</section>
<!-- //Team -->



<section class="copyright py-4">
    <div class="agileits_w3layouts-copyright text-center">
        <p>© 2021 E-Reading System. </a>
        </p>
    </div>
</section>
<!-- //footer -->



<!-- js-scripts -->

<!-- js -->
<script type="text/javascript" src="plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>

<!-- start-smoth-scrolling -->
<script src="login_asset/js/SmoothScroll.min.js"></script>
<script type="text/javascript" src="login_asset/js/move-top.js"></script>
<script type="text/javascript" src="login_asset/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<script type="text/javascript">

    $("#login").click(function(e){

        var id = $("#student_id").val();
        var pw = $("#pw").val();

        e.preventDefault();
        $.ajax({
            url:"ajax/login_student.php",
            method:"POST",
            data:{
                id:id,
                pw:pw,
            },
            success: function(data){
                if(data == 1){
                    window.location.replace("dashboard.php");
                }else{
                    alert("You dont have the credential to access this site");
                }
            }
        });
    });

</script>

<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {lat: -25.344, lng: 131.036};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>

</body>
</html>