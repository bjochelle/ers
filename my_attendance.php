<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">My Attendance</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Event Name</th>
                  <th>Place</th>
                  <th>Date Added</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">

  function get_attendance(){


     var user_id = $("#user_id").val();

  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/get_my_attendance.php",
      "dataSrc":"data",
      "data":{
        user_id:user_id
      }
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"place"
      },
      {
        "data":"date"
      },
    ]
  });
}
  
$(document).ready(function (){
  get_attendance();
});
</script>