<style type="text/css">
  @media print {
    body {
  font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
  font-size: 1em;
  color: #333333;
  margin-top: 2cm;
  margin-right: 2cm;
  margin-bottom: 1.5cm;
  margin-left: 2cm
}

  #report{
    margin-top: 10px;
  }
        }

  .pull-right{
    float: right;
  }
</style>
<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Score Report </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">

                <div class="col-sm-4 input-group" style="margin-top:10px"> 
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Quiz Name: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                    <select class='form-control' id="quiz_id" style="text-transform: capitalize;margin-right: 10px;">
                      <option value="">-- Select Quiz --</option>
                      <?php 

                      include "core/config.php";

                      $event = $connectDB->query("SELECT * from quiz_header ");
                     while($row = mysqli_fetch_array($event)){ ?>
                              <option value="<?php echo $row['quiz_id'];?>"><?php echo $row['quiz_name'] ?></option>

                      <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 input-group" style="margin-top:10px"> 
                  <div class="input-group-prepend">
                    <span class="input-group-text"><strong> Year : <span style="color:red;">*</span></span></strong>
                  </div>
                     <select class='form-control' id="sy" style="text-transform: capitalize;margin-right: 10px;" onchange="getSection()">
                      <option value="">-- Select Year --</option>
                      <?php 

                      include "core/config.php";

                      $event = $connectDB->query("SELECT * from tbl_student group by year ");
                     while($row = mysqli_fetch_array($event)){ ?>
                              <option value="<?php echo $row['year'];?>"><?php echo $row['year'] ?></option>

                      <?php } ?>
                    </select>
                </div>

                <div class="col-sm-4 input-group" style="margin-top:10px"> 
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Section: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                   <select class='form-control' id="section" style="text-transform: capitalize;margin-right: 10px;">
                     <option value="">-- Select Year --</option>
                    </select>
                </div>
                <div class="col-sm-12" style="margin-top:10px"> 
                  <button class="btn btn-primary btn-sm pull-right" onclick="gen()" id="btn_gen"><span class="fa fa-refresh"></span> Generate </button>

                  <button class="btn btn-default btn-sm pull-right"  onclick="myFunction()" ><span class="fa fa-print"></span> Print </button>
                </div>      
              </div>
            
              <div class="card-body" id="report">

            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php include "footer.php";?>
  <script src="dist/js/jquery.PrintArea.js"></script>

  <script type="text/javascript">
    function gen() {
        var quiz_id = $("#quiz_id").val();
        var sy = $("#sy").val();
        var section = $("#section").val();

    if (section == "" || quiz_id == "" || section == ""){
      alert ("Please fill in the form");
    }else{

      $("#btn_gen").prop('disabled', true);
      $("#btn_gen").html("<span class='fa fa-spinner fa-spin'></span> Loading ...");

     $.ajax({
        type:"POST",
        url:"ajax/report.php",
        data:{
          quiz_id:quiz_id,
          sy:sy,
          section:section
        },
        success:function(data){
             $("#report").html(data);
      
          $("#btn_gen").prop('disabled', false);
          $("#btn_gen").html("<span class='fa fa-refresh'></span> Generate");
        }
      });
      }
     
    }

    function myFunction() {
    var mode = 'iframe'; // popup
    var close = mode == "popup";
    var options = { mode : mode, popClose : close};
    $("#report").printArea( options );

}

function getSection(){

  var sy = $("#sy").val();
   $.post("ajax/getSection.php", {
                sy: sy
            },
            function (data) {
              $("#section").html(data);
     });
}
  </script>