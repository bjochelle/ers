<?php 
include "core/config.php";
$checkNumEventsSQL = $connectDB->query("SELECT * FROM quiz_header where quiz_id='$_GET[id]'");
$rowEvents = mysqli_fetch_array($checkNumEventsSQL);

$subject = mysqli_fetch_array($connectDB->query("SELECT * FROM subjects where subject_id='$rowEvents[subject_id]'"));
$teacher = mysqli_fetch_array($connectDB->query("SELECT * FROM tbl_user where user_id='$rowEvents[user_id]'"));

if($rowEvents['module_id'] != 0){
  $module = mysqli_fetch_array($connectDB->query("SELECT module_file FROM modules where module_id='$rowEvents[module_id]'"));

  $file = $module['module_file'];
}else{
  $file = "";
}

?>
<?php include "header.php";?>
<link href="AVRecorder/av-recorder.css" rel="stylesheet">
<!-- Progress Loader CSS -->
<link href="AVRecorder/loader.css" rel="stylesheet">

<!-- RecorderJS library Jul 20, 2016 release -->
<script src="recorderJs/recorder.js"></script>

<!-- jQuery AV Recorder JS -->
<!-- <script src="./AVRecorder/media-recorder-flash.js"></script>  DROPPING FLASH SUPPORT -->
<script src="AVRecorder/av-recorder-api.js"></script>
<!-- <script src="AVRecorder/av-recorder-html5.js"></script> -->
<script src="AVRecorder/av-recorder.js"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">TAKE READING EXERCISE </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
          <div class="row">
              <div class='col-sm-6'>
              <div class="card-body">
                <div class="form-row align-items-center">
                    <div class="col-md-12">
                      <h6 class="input-group mb-2" style="color:red;">
                          Instruction  : You have an accumulated time of 10 mins. After the accumulated time, it will save automatically your voice record . <br>Please think carefully before you click "record" .
                      </h6>
                    </div>
                    <div class="col-md-4">
                      <h5 class="input-group mb-2">
                          Subject : <?=$subject['subject_name'];?> (<?=$subject['subject_code'];?>) 
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <h5 class="input-group mb-2">
                          Level :  <?=$rowEvents['level'];?> 
                      </h5>
                    </div>
                    <div class="col-md-4">
                      <h5 class="input-group mb-2">
                          Teacher :  <?=$teacher['fname'];?> <?=$teacher['lname'];?> 
                      </h5>
                    </div>

                  </div>
                </div>
                        <!-- Small boxes (Stat box) -->


                <div class="card container">
                    <div class="card-body">
                      <h5 class="card-title" style="text-align: center;"> <?=$rowEvents['quiz_name'];?> </h5><br>
                      <article>
                        <p><?=$rowEvents['quiz_data'];?></p>
                      </article>
                   
                    </div>
                  <div class="col-md-7 col-md-offset-3" style="margin: auto;">
                      <br/>
                      <div id="avRecorder-fallback-ajax-wrapper"></div>
                      <div id="avRecorder" class=""></div>
                  </div>
                </div>
              </div>
              <div class='col-sm-6'>
                <?php
                if($file == ""){ ?>
                <h4 style='text-align:center;color:red' >NO MODULE FILE ATTACHED</h4>
              <?php }else { ?> 
                <iframe height="100%" width=100% src='pdf/lib/web/viewer.html?file=../../../ajax/uploads/<?=$file?>'></iframe>
              <?php }  ?>
              
              </div>
          </div>
      </div>
    </section>
  </div>
  
  <script type="text/javascript">
  
    $(document).ready(function(){
      createRecorder();

      var browserUserAgent = navigator.userAgent;
      if(browserUserAgent.includes("Safari") == true && browserUserAgent.includes("Chrome") == false){
        $("#audioVideoControl option")[1].remove();
      }else if(browserUserAgent.includes("Edge") == true || browserUserAgent.includes("MSIE ") == true || browserUserAgent.includes("Trident") == true){
        $("#audioVideoControl option")[1].remove();
      }
      
      $("#audioVideoControl").change(function(){
          audio = true;
          video = false;
          window.location = "demo.php";
      });
      
      $("#avRecorder").bind('uploadFinished', function (event, data) {

        $(".av-recorder-controls").hide();
        $(".av-recorder-meter").hide();
        $(".panel-heading").hide();
         success_add();

       
        console.log("uploadFinished");
        
      });
    });
  
    function createRecorder(){
      var id = "<?php echo $_GET['id'];?>";
      $("#avRecorder").AvRecorder('avRecorder',{
            constraints: {
              audio: true,
                video: false,
                video_resolution: "320"
            },
            file: null,
            time_limit: "900",
            server_upload_endpoint: "ajax/demoRecordFile.php?id="+id //Will be appended to the window.orign that the request is coming from.
          });
    };
    
  </script>
</body>

</html>