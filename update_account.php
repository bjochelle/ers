<?php include "header.php";

   $id= $_SESSION['id'];
  include "core/config.php";

  $query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_user where user_id = '$id'"));

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Accout</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row" id="dashboard">
          
          <div class="col-md-6">

            <div class="card card-danger">
              <div class="card-header" style="background: #0f7b0a  !important;">
                <h3 class="card-title">My Information</h3>
              </div>
              <div class="card-body">
                <!-- Date dd/mm/yyyy -->
                <div class="form-group">
                   <input type="hidden" class="form-control" name="id" value="<?php echo $query['user_id'];?>" id="id">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> First Name : <span style="color:red"> * </span> </span>
                    </div>
                    <input type="text" class="form-control" name="fname" value="<?php echo $query['fname'];?>" id="fname">
                  </div>

                   
                  <!-- /.input group -->
                  <!-- /.input group -->
                </div>

                 <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Last Name : <span style="color:red"> * </span> </span>
                    </div>
                    <input type="text" class="form-control" name="lname" value="<?php echo $query['lname'];?>" id="lname">
                  </div>
                      </div>
                <!-- /.form group -->

                <!-- Date mm/dd/yyyy -->
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Username : <span style="color:red"> * </span> </span>
                    </div>
                    <input type="text" class="form-control" value="<?php echo $query['un'];?>" id="un">
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->

                 <!-- Date mm/dd/yyyy -->
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Current Password : <span style="color:red"> * </span></span>
                    </div>
                    <input type="password" class="form-control" name="curr_pw" placeholder="Please Enter your Current Password" id="curr_pw">
                  </div>
                  <!-- /.input group -->
                </div>

                 <!-- Date mm/dd/yyyy -->
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> New Password : <span style="color:red"> * </span></span>
                    </div>
                    <input type="password" class="form-control" placeholder="Please Enter your New Password" name="new_pw" id="new_pw">
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
                <button type="submit" class="btn btn-success float-right" id="btn_update"><span class="fa fa-check-circle"></span> Update</button>


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->


          </div>
          
          <!-- ./col -->
         
          <!-- ./col -->
        </div>
        
        <!-- /.row -->

        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php include "footer.php";?>

  <script type="text/javascript">
  
$(document).ready(function (){
  $("#btn_update").click(function(){

    var id = $("#id").val();
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var username = $("#un").val();
    var curr_pw = $("#curr_pw").val();
    var new_pw = $("#new_pw").val();
    // alert(id + admin_name + username + curr_pw + new_pw);

    if (fname == "" || lname == ""  || username == "" || curr_pw == "" || new_pw == ""){
      alert ("Please Fill In the Form");
    }else{

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
  
    $.ajax({
      url:"ajax/update_admin.php",
      method:"POST",
      data:{
          id:id,
          fname:fname,
          lname:lname,
          username:username,
          curr_pw:curr_pw,
          new_pw:new_pw
      },
      success: function(data){
          if(data == 1){
         success_update();
       } else if(data == 2){
         alert ("Wrong Current Password");
        }else{
         
          failed_query();
        }

 
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Update ");
      }
    });
    }

  });
});

  
  </script>