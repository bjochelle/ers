<?php 
include "core/config.php";
$checkNumEventsSQL = $connectDB->query("SELECT * FROM quiz_header where quiz_id='$_GET[id]'");
$rowEvents = mysqli_fetch_array($checkNumEventsSQL);

$subject = mysqli_fetch_array($connectDB->query("SELECT * FROM subjects where subject_id='$rowEvents[subject_id]'"));
$teacher = mysqli_fetch_array($connectDB->query("SELECT * FROM tbl_user where user_id='$rowEvents[user_id]'"));

?> 
<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Quiz</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
     <form id="edit_quiz">
  <div class="form-row align-items-center">

     <input type="hidden" class="form-control" id="inlineFormInputGroup" name="quiz_id"  value="<?php echo $_GET['id'];?>">

    <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Quiz Name</div>
        </div>
        <input type="text" class="form-control" id="inlineFormInputGroup" name="quiz_name" placeholder="Quiz Name" value="<?=$rowEvents['quiz_name'];?>">
      </div>
    </div>

    <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Subject</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="subject_id" value="<?=$rowEvents['subject_id'];?>">
        <!-- Page specific script -->

        <?php 
        include "core/config.php";
        $check = $connectDB->query("SELECT * FROM subjects");
         while($row = mysqli_fetch_array($check)){

          if($row['subject_id'] === $rowEvents['subject_id']){
              $selected = 'selected';
          }else{
              $selected = '';
          }
        ?> 
        <option value="<?=$row['subject_id']?>" <?=$selected;?>>  <?=$row['subject_name']?> (<?=$row['subject_code']?>)</option>
          <?php }?>
      </select>
      </div>
    </div>

     <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Level</div>
        </div>
          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="level">
            <option selected>Choose...</option>
            <option value="easy" <?php  if($rowEvents['level'] === 'easy'){ echo 'selected';}?>>Easy</option>
            <option value="medium" <?php  if($rowEvents['level'] === 'medium'){ echo 'selected';}?>>Medium</option>
            <option value="hard" <?php  if($rowEvents['level'] === 'hard'){ echo 'selected';}?>>Hard</option>
          </select>
        </div>
      </div>
      <div class="col-sm-3" style="margin-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">Deadline</div>
          </div>
        <input type='date' value='<?=$rowEvents['deadline']?>' name='deadline' class='form-control'>
        </div>
      </div>    


<div class="col-sm-12" style="margin-top: 10px">
    <textarea class="form-control" rows="10" cols="125" name="quiz_data"> <?=$rowEvents['quiz_data']?> </textarea>
 </div>

  </div>
      <div class="col-sm-12" style="margin-top: 10px">
      <button type="submit" class="btn btn-primary pull-right" id="btn_add">Update</button>
    </div>
</form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    

  $("#edit_quiz").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/edit_quiz.php",
      method:"POST",
      data:$("#edit_quiz").serialize(),
      success: function(data){
        if(data == 1){
         success_add();

        setTimeout(function(){
           window.location.href = "quiz.php";
         },1500)
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddFaculty").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });

</script>