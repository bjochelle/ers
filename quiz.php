<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reading Exercise</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="padding: 20px;">
              <?php if($user_type === "T"){ ?>
              <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Create Reading </button>
               <?php }?>
          </div>
          <?php require 'modals/modal_upload_faculty.php'; ?>
          <?php require 'modals/modal_addFaculty.php';?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Reading Name</th>
                  <th>Subject</th>
                  <th>Level</th>
                  <th>Created By</th>
                  <th>Deadline</th>
                  <th>Takers</th>
                  <th style="width: 20%;">Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    
$("#btn_download").click(function(e){
  e.preventDefault();
  var dl = confirm("Download Template for Teacher(csv file).");
  if(dl == true){
    window.location = 'ajax/download_template_teacher.php?bid=';
  }
  
});

    function showUploadModal(){
  $("#modalStudent").modal('show');
}

function showAddModal(){
  window.location.href = "createQuiz.php";
}


$("#upload_faculty").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_template_faculty.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      $("#modalStudent").modal("hide");
      $("#modalResponse").modal('show');
      $("#response").html(data);
      
      get_Faculty();
    }
  });
});


  $("#add_faculty").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_faculty.php",
      method:"POST",
      data:$("#add_faculty").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_add();
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddFaculty").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_student").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_student.php",
      method:"POST",
      data:$("#update_student").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_update();
        }else if(data == 2){
          failed_query();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });

  function closeopenstatus(id){
      $("#closeopen_btn").prop("disabled", true);
      $("#closeopen_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
      $.post("ajax/closeopenQuiz.php", {
        id: id
      }, function(data){
        if(data > 0){
          success_update();
        }else{
          failed_query();
        }
        get_Faculty();
      })
  }

  function get_Faculty(){
  var table = $('#example1').DataTable();
  table.destroy();

  var user_type = "<?php echo $user_type; ?>";

  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/quiz_dt.php",
      "dataSrc":"data",
      "type":"POST",
      "data":{
          user_type:user_type,
        }
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"quiz_name"
      },
      {
        "data":"subject"
      },
      {
        "data":"level"
      },
      {
        "data":"teacher"
      },
      {
        "data":"deadline"
      },
      {
        "mRender": function(data,type,row){

          var viewQuiztakers = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Edit' value='" + row.id+ "' onclick='window.open(\"quiz_takers.php?id=" + row.id +"\")'><span class='fa fa-eye'></span> View</button>";
    
          return "<center>"+viewQuiztakers+"</center>";
        }
      },
      {
        "mRender": function(data,type,row){

           if(user_type === 'T'){
              var edit = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Edit' value='" + row.id+ "' onclick='window.open(\"editQuiz.php?id=" + row.id +"\")'><span class='fa fa-pencil'></span> Edit</button>";
                var read = '';
          }else if(user_type === 'A'){
              var read = '';
                 var edit = '';

          }else{
            if(row['allow'] === 0){
              var read = "<button class='btn btn-success btn-sm' data-toggle='tooltip' title='Take Quiz' value='" + row.id+ "' onclick='window.open(\"takeQuiz.php?id=" + row.id +"\")'><span class='fa fa-university'></span> Take Quiz </button>";
            }else{
              var read = '';
          
            }
                var edit = '';
          }

        if(user_type === 'T'){
          if(row.status == 0){
              var closeopen = "<button id='closeopen_btn"+row.id+"' class='btn btn-danger btn-sm' data-toggle='tooltip' title='Close' value='" + row.id+ "' onclick='closeopenstatus("+row.id+")'><span class='fa fa-eye-slash'></span> Close</button>";
          }else{
             var closeopen = "<button id='closeopen_btn"+row.id+"' class='btn btn-success btn-sm' data-toggle='tooltip' title='Open' value='" + row.id+ "' onclick='closeopenstatus("+row.id+")'><span class='fa fa-eye'></span> Open </button>";
          }
        }else{
            var closeopen = '';
        }

          return "<center>"+edit+read+closeopen+"</center>";
        }
      }
    ]
  });
}
  
$(document).ready(function (){
  get_Faculty();
});
</script>