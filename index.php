﻿
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Manlucahoc Elementary School</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Educational Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--// Meta tag Keywords -->
    <!-- css files -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //css files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet">
    <!-- //online-fonts -->
</head>
<body>
<div class="main-w3layouts" id="home">


    <!-- //Modal1 -->

    <!-- Modal3 -->
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content news-w3l">
                <div class="modal-header">
                    <button type="button" class="close w3l" data-dismiss="modal">&times;</button>
                    <h4>Login Your Account</h4>
                    <!--newsletter-->
                    <div class="login-main wthree">
                    <label class="col-sm-8 col-sm-offset-4">Login Options: </label>
                            <select name="loginOption" id="loginOption" onchange="loginOptions(this)" class='form-control'>
                                <option value='s'> Student </option>
                                <option value="a"> Administrator / Teacher</option>
                            </select>

                            <br>
                        <div id='student-form'>
                            <form action="#" id='studentLogin' method="post" >
                                
                        
                                <div class='col-sm-12'>
                                    <input type="number" id="student_id" name="student_id" placeholder="Identification number">

                                    <input type="password" placeholder="Password" name="pw" id="pw">
                                    <input type="submit" value='Login'  id="loginS">
                                </div>
                            </form>
                        </div>
                        <div id='admin-form' style='display: none'>
                            <form action="#" id='adminLogin' method="post">
                                
                    
                                <div class='col-sm-12'>
                                    <input type="text" name="username" placeholder="Username" required="" autocomplete="off" />

                                    <input type="password" name="password" placeholder="Password" required="" id="myInput" />
                                    <input type="submit"  value="Login" id="loginA">
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-8 col-sm-offset-3 w3ls-login  w3l-sub animated " id="notif" style="color: #e91e63;"> </div>
                    </div>
                    <!--//newsletter-->
                </div>
            </div>
        </div>
    </div>


    <!--//top-bar-->
    <!-- navigation -->
    <div class="top-nav" style="top:0% !important;">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
            <!-- navbar-header -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" class="hvr-underline-from-center"> <image src="images/logo_2.png" style='width: 50px;height: auto;' alt="LOGO"></image></a></li>
                    <li><a href="#" class="hvr-underline-from-center active">Home</a></li>
                    <li><a href="#about" class="hvr-underline-from-center scroll">About Us</a></li>
                    <li><a href="#services" class="hvr-underline-from-center scroll">Services</a></li>
                    <li><a href="#gallery" class="hvr-underline-from-center scroll">Gallery</a></li>
<!--                    <li><a href="#team" class="hvr-underline-from-center scroll">Our Team</a></li>-->
                    <li><a href="#events" data-toggle="modal" data-target="#myModal3" class="hvr-underline-from-center scroll">Login</a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </nav>
    </div>
    <!-- //navigation -->

    <div class="header">
        <!-- Slider -->
        <div class="slider">
            <div class="callbacks_container">
                <ul class="rslides" id="slider">
                    <li>

                        <div class="slider-info">
                            <p>wisdom begins with wonder.</p>
                            <h3><a><span>Edu</span> cational</a></h3>
                            <h6>wisdom begins with wonder.</h6>
                        </div>
                    </li>
                    <li>

                        <div class="slider-info">
                            <p>Education is a vaccine for violence.</p>
                            <h3><a><span>Edu</span> cational</a></h3>
                            <h6>wisdom begins with wonder.</h6>
                        </div>
                    </li>
                    <li>

                        <div class="slider-info">
                            <p>wisdom begins with wonder.</p>
                            <h3><a><span>Edu</span> cational</a></h3>
                            <h6>wisdom begins with wonder.</h6>
                        </div>
                    </li>
                    <li>

                        <div class="slider-info">
                            <p>Learning never exhausts the mind.</p>
                            <h3><a><span>Edu</span> cational</a></h3>
                            <h6>wisdom begins with wonder.</h6>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- //Slider -->
    </div>
</div>
<!--main-content-->

<!--about-->
<div id="about" class="about">
    <div class="container">
        <h1>About <span>us</span></h1>
        <h2>Manlucahoc Elementary School</h2>
        <div class="specialty-grids-top">
            <div class="col-md-4 service-box" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <figure class="icon">
                    <span class="glyphicon glyphicon-education a1" aria-hidden="true"></span>
                </figure>
                <h5>Curricular Class</h5>
                <p>Kinder & Elementary</p>
            </div>
            <div class="col-md-4 service-box wow bounceIn animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <figure class="icon">
                    <span class="glyphicon glyphicon-home a2" aria-hidden="true"></span>
                </figure>
                <h5>School Address</h5>
                <p>Manlucahoc Elementary School, Sipalay City, Negros Occidental</p>
            </div>
            <div class="col-md-4 service-box wow bounceIn animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                <figure class="icon">
                    <span class="glyphicon glyphicon-barcode a3" aria-hidden="true"></span>
                </figure>
                <h5>School ID </h5>
                <p>117377</p>
            </div>
            <div class="clearfix"> </div>
        </div>

        <!-- Modal1 -->
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" >
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content abt-w3l">
                    <div class="modal-header">
                        <button type="button" class="close clo1" data-dismiss="modal">&times;</button>
                        <h4>Enter the Following Details</h4>
                        <form action="#" method="post" class="mod2">
                            <ul>
                                <li class="text">Name of Applicant :  </li>
                                <li class="agileits-main"><input name="name" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">Date of Birth :  </li>
                                <li class="agileits-main"><input name="organization" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">Father Name  :  </li>
                                <li class="agileits-main"><input name="name" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">Gender  :  </li>
                                <li class="agileits-main"><input name="contact" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">mobile no  :  </li>
                                <li class="agileits-main"><input name="mobile" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">Address  :  </li>
                                <li class="agileits-main"><input name="mobile" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">District  :  </li>
                                <li class="agileits-main"><input name="mobile" type="text" required></li>
                            </ul>
                            <ul>
                                <li class="text">State  :  </li>
                                <li class="agileits-main"><input name="mobile" type="text" required></li>
                            </ul>
                            <div class="clear"></div>
                            <div class="agile-submit">
                                <input type="submit" value="submit">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- //Modal1 -->
        <div class="clearfix"></div>
    </div>
</div>
<!--<div class="about-info">-->
<!--    <img src="images/ab.jpg" alt="about-image">-->
<!--    <div class="right-agile">-->
<!--        <h5>Educational</h5>-->
<!--        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
<!--        <h5>Educational</h5>-->
<!--        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam. </p>-->
<!--    </div>-->
<!--</div>-->
<div class="clearfix"></div>
<!--//about-->

<!-- services -->
<div class="services" id="services">
    <h3>Services</h3>
    <div class="col-md-4 services-gd text-center" data-wow-delay=".5s">
        <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
            <a class="hi-icon"><img src="images/book.png" alt=" " /></a>
        </div>
        <h4>Learn</h4>
    </div>
    <div class="col-md-4 services-gd text-center" data-wow-delay=".5s">
        <div class="hi-ion-wrap hi-icon-effect-9 hi-icon-effect-9a">
            <a class="hi-icon"><img src="images/edu.png" alt=" " /></a>
        </div>
        <h4>Educate</h4>
    </div>
    <div class="col-md-4 services-gd text-center" data-wow-delay=".5s">
        <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
            <a class="hi-icon"><img src="/images/uni.png" alt=" " /></a>
        </div>
        <h4>Socialize</h4>
    </div>
    <!--    <div class="col-md-2 services-gd text-center" data-wow-delay=".5s">-->
    <!--        <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">-->
    <!--            <a href="#" class="hi-icon"><img src="images/hourglass.png" alt=" " /></a>-->
    <!--        </div>-->
    <!--        <h4>Service 4</h4>-->
    <!--        <p>Lorem Ipsum</p>-->
    <!--    </div>-->

    <div class="clearfix"> </div>
</div>
<!-- //services -->

<!-- gallery -->
<div class="portfolio" id="gallery">
    <h3>Gallery</h3>

    <div class="portfolio-top wow fadeInDown animated" data-wow-delay=".5s">

        <div class="col-md-4 grid slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal1"  data-toggle="modal">

                    <img style='height: 316px;object-fit: cover;' src="images/m_1.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4>Educational</h4>
                        <p> Education is not a problem. Education is an opportunity.</p>
                    </figcaption>
                </a>
            </figure>
        </div>
        <div class="col-md-4 grid slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal2"  data-toggle="modal">

                    <img style='height: 316px;object-fit: cover;' src="images/m_2.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4>Educational</h4>
                        <p> Education is not a problem. Education is an opportunity.</p>
                    </figcaption>
                </a>
            </figure>
        </div>
        <div class="col-md-4 grid slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal3"  data-toggle="modal">

                    <img style='height: 316px;object-fit: cover;' src="images/m_3.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4>Educational</h4>
                        <p> Education is not a problem. Education is an opportunity.</p>
                    </figcaption>
                </a>
            </figure>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="portfolio-top wow fadeInUp animated" data-wow-delay=".5s">
        <!-- <div class="col-md-3 grid grid-wi slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal4"  data-toggle="modal">

                    <img src="images/g4.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4 class="effcet-text"> Educational</h4>
                        <p> Learning is never done without errors and defeat.</p>
                    </figcaption>
                </a>
            </figure>
        </div> -->
        <!-- <div class="col-md-3 grid grid-wi slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal5"  data-toggle="modal">

                    <img src="images/g5.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4 class="effcet-text"> Educational</h4>
                        <p>Learning is never done without errors and defeat.</p>
                    </figcaption>
                </a>
            </figure>
        </div> -->
       <!--  <div class="col-md-3 grid grid-wi slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal6"  data-toggle="modal">

                    <img src="images/g6.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4 class="effcet-text">Educational</h4>
                        <p>Learning is never done without errors and defeat.</p>
                    </figcaption>
                </a>
            </figure>
        </div> -->
       <!--  <div class="col-md-3 grid grid-wi slideanim">
            <figure class="effect-jazz">
                <a href="#portfolioModal7"  data-toggle="modal">

                    <img src="images/g7.jpg" alt="img25" class="img-responsive"/>
                    <figcaption>
                        <h4 class="effcet-text"> Educational</h4>
                        <p> Learning is never done without errors and defeat.</p>
                    </figcaption>
                </a>
            </figure>
        </div> -->
        <div class="clearfix"></div>
    </div>
</div>
<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade slideanim" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/m_1.jpg" class="img-responsive" alt="">
                    <p></p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/m_2.jpg" class="img-responsive" alt="">
                    <p></p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/m_3.jpg" class="img-responsive" alt="">
                    <p></p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/g4.jpg" class="img-responsive" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/g5.jpg" class="img-responsive" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/g6.jpg" class="img-responsive" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">

            <div class="col-lg-8 col-lg-offset-2">
                <div class="modal-body">
                    <h3>Educational</h3>

                    <img src="images/g7.jpg" class="img-responsive" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /Portfolio Modals -->
<!-- //gallery -->

<!-- team -->
<!--<div class="team" id="team">-->
<!--    <div class="container">-->
<!--        <h3 class="title">Our  <span>Team</span></h3>-->
<!--        <div class="team_gds">-->
<!--            <div class="col-md-3 team_gd1 wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">-->
<!--                <div class="team_pos">-->
<!--                    <div class="team_back"></div>-->
<!--                    <img class="img-responsive" src="images/t1.jpg"  alt=" ">-->
<!--                    <div class="team_info">-->
<!--                        <a href="#"  class="face_one"><i class=" so1 fa fa-facebook fc1" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so2 fa fa-twitter fc2" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so3 fa fa-google fc3" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <h4>Victoria</h4>-->
<!--                <p>Lorem Ipsum</p>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-3 team_gd1 wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">-->
<!--                <div class="team_pos">-->
<!--                    <div class="team_back"></div>-->
<!--                    <img class="img-responsive" src="images/t2.jpg"  alt=" ">-->
<!--                    <div class="team_info">-->
<!--                        <a href="#"  class="face_one"><i class=" so1 fa fa-facebook fc1" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so2 fa fa-twitter fc2" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so3 fa fa-google fc3" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <h4>Patrick</h4>-->
<!--                <p>Lorem Ipsum</p>-->
<!--            </div>-->
<!--            <div class="col-md-3 team_gd1 wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">-->
<!--                <div class="team_pos">-->
<!--                    <div class="team_back"></div>-->
<!--                    <img class="img-responsive" src="images/t3.jpg"  alt=" ">-->
<!--                    <div class="team_info">-->
<!--                        <a href="#"  class="face_one"><i class=" so1 fa fa-facebook fc1" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so2 fa fa-twitter fc2" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so3 fa fa-google fc3" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <h4>Thompson</h4>-->
<!--                <p>Lorem Ipsum</p>-->
<!--            </div>-->
<!--            <div class="col-md-3 team_gd1 wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">-->
<!--                <div class="team_pos">-->
<!--                    <div class="team_back"></div>-->
<!--                    <img class="img-responsive" src="images/t4.jpg"  alt=" ">-->
<!--                    <div class="team_info">-->
<!--                        <a href="#"  class="face_one"><i class=" so1 fa fa-facebook fc1" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so2 fa fa-twitter fc2" aria-hidden="true"></i></a>-->
<!--                        <a href="#"  class="face_one"><i class=" so3 fa fa-google fc3" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <h4>Federica</h4>-->
<!--                <p>Lorem Ipsum</p>-->
<!--            </div>-->
<!--            <div class="clearfix"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- //team -->

<!-- Events -->
<!--<div class="event" id="events">-->
<!--    <div class="container">-->
<!--        <h3>Events</h3>-->
<!--        <div class="col-md-4 eve-agile e1">-->
<!--            <div class="eve-sub1">-->
<!--                <a href="#" data-toggle="modal" data-target="#myModal5"><img src="images/e2.jpg" alt="image"></a>-->
<!--                <h4><a href="#" data-toggle="modal" data-target="#myModal5">Educational</a></h4>-->
<!--                <h6>By an <a href="#">admin</a>, Oct-2016</h6>-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>-->
<!--            </div>-->
<!--            <div class="eve-sub2">-->
<!--                <div class="eve-w3lleft">-->
<!--                    <h6><i class="fa fa-comment-o" aria-hidden="true"></i>17</h6>-->
<!--                    <h6><i class="fa fa-heart-o" aria-hidden="true"></i>78</h6>-->
<!--                </div>-->
<!--                <div class="eve-w3lright e1">-->
<!--                    <a href="#" data-toggle="modal" data-target="#myModal5"><h5>More</h5></a>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-md-4 eve-agile e2">-->
<!--            <div class="eve-sub1">-->
<!--                <a href="#" data-toggle="modal" data-target="#myModal6"><img src="images/e1.jpg" alt="image"></a>-->
<!--                <h4><a href="#" data-toggle="modal" data-target="#myModal6">Educational</a></h4>-->
<!--                <h6>By an <a href="#">admin</a>, Oct-2016</h6>-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>-->
<!--            </div>-->
<!--            <div class="eve-sub2">-->
<!--                <div class="eve-w3lleft">-->
<!--                    <h6><i class="fa fa-comment-o" aria-hidden="true"></i>64</h6>-->
<!--                    <h6><i class="fa fa-heart-o" aria-hidden="true"></i>86</h6>-->
<!--                </div>-->
<!--                <div class="eve-w3lright">-->
<!--                    <a href="#" data-toggle="modal" data-target="#myModal6"><h5>More</h5></a>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-md-4 eve-agile e3">-->
<!--            <div class="eve-sub1">-->
<!--                <a href="#" data-toggle="modal" data-target="#myModal7"><img src="images/e3.jpg" alt="image"></a>-->
<!--                <h4><a href="#" data-toggle="modal" data-target="#myModal7">Educational</a></h4>-->
<!--                <h6>By an <a href="#">admin</a>, Oct-2016</h6>-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>-->
<!--            </div>-->
<!--            <div class="eve-sub2">-->
<!--                <div class="eve-w3lleft">-->
<!--                    <h6><i class="fa fa-comment-o" aria-hidden="true"></i>47</h6>-->
<!--                    <h6><i class="fa fa-heart-o" aria-hidden="true"></i>58</h6>-->
<!--                </div>-->
<!--                <div class="eve-w3lright">-->
<!--                    <a href="#" data-toggle="modal" data-target="#myModal7"><h5>More</h5></a>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Educational</h4>
                <img src="images/e2.jpg" alt="blog-image" />
                <span>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem , eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.accusantium doloremque laudantium.</span>
            </div>
        </div>

    </div>
</div>
<!-- //Modal1 -->
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Educational</h4>
                <img src="images/e1.jpg" alt="blog-image" />
                <span>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem , eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.accusantium doloremque laudantium.</span>
            </div>
        </div>

    </div>
</div>

<!-- //Modal1 -->
<div class="modal fade" id="myModal7" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Educational</h4>
                <img src="images/e3.jpg" alt="blog-image" />
                <span>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem , eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.accusantium doloremque laudantium.</span>
            </div>
        </div>

    </div>
</div>
<!-- //Modal1 -->

<!-- //Events -->


<!--contact-->
<div class="map">

    <iframe style="height: 500px;width: 100%;" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&q=9.719607,122.491253&maptype=satellite&zoom=16" allowfullscreen></iframe>

</div>


<!-- footer -->
<div class="footer" id="footer">
    <div class="container">
        <!--        <div class="col-md-4 agileinfo_footer_grid">-->
        <!--            <h4>About Us</h4>-->
        <!--            <p>  <ul>-->
        <!--                <li>School ID : 117377</li>-->
        <!--                <li>School Name : Manlucahoc Elementary School </li>-->
        <!--                <li>School Address : Manlucahoc Elementary School, Sipalay City, Negros Occidental </li>-->
        <!--                <li>Municipality : SIPALAY CITY</li>-->
        <!--                <li>Region : NIR</li>-->
        <!--                <li>Province : Negros Occidental </li>-->
        <!--                <li>Division : Negros Occidental </li>-->
        <!--                <li>Legistative District : 6th District </li>-->
        <!--                <li>Curricular Class : Kinder & Elementary</li>-->
        <!--                <li>Date of Operation : Wednesday, January 01, 1964</li>-->
        <!--                <li>District : Sipalay II</li>-->
        <!--                <li>Classification : Local Government</li>-->
        <!--                <li>School Type : School with no Annexes</li>-->
        <!--                <li>Class Organization : Monograde</li>-->
        <!--            </ul></p>-->
        <!--        </div>-->
        <div class="col-md-4 agileinfo_footer_grid mid-w3l nav2">
            <h4>Options</h4>
            <ul>
                <li><a href="#home" class="scroll">Home</a></li>
                <li><a href="#about" class="scroll">About Us</a></li>
                <li><a href="#services" class="scroll">Services</a></li>
                <li><a href="#gallery" class="scroll">Gallery</a></li>
<!--                <li><a href="#team" class="scroll">Team</a></li>-->
                <!--                <li><a href="#events" class="scroll">Events</a></li>-->
                <!--                <li><a href="#testimonials" class="scroll">Testimonials</a></li>-->
            </ul>
        </div>
        <div class="col-md-4 agileinfo_footer_grid">
            <h4>Address</h4>
            <ul>
                <li><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Manlucahoc Elementary School, Sipalay City, Negros Occidental</li>
                <li><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span><a href="#">117377</a></li>
                <li><span class="glyphicon glyphicon-education" aria-hidden="true"></span> Kinder & Elementary</li>
            </ul>
        </div>


    </div>
</div>
<!-- //footer -->

<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>


<script src="js/jquery.chocolat.js"></script>
<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
<!--light-box-files -->
<script>
    $(function() {
        $('.gallery-grid a').Chocolat();
    });
</script>
<!-- required-js-files-->

<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            items : 1,
            lazyLoad : true,
            autoPlay : true,
            navigation : false,
            navigationText :  false,
            pagination : true,
        });
    });
</script>
<!--//required-js-files-->

<script src="js/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            pager:false,
            nav: true,
            speed: 1000,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
</script>


<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->

<!-- bottom-top -->
<!-- smooth scrolling -->
<script type="text/javascript">
      $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */
        $().UItoTop({ easingType: 'easeOutQuart' });

        loginOptions(option);
    });
    function loginOptions(option){
        if(option.value == 's'){
            $("#student-form").css("display","block");
            $("#admin-form").css("display","none");
        }else{
            $("#student-form").css("display","none");
            $("#admin-form").css("display","block");
        }
    }

    $("#studentLogin").on("submit", function(e){
        e.preventDefault();

        var url = "ajax/login_student.php";
        var data = $(this).serialize();
        $.post(url, data, function(res){
     
            if(res == 1){
                window.location.replace("dashboard.php");
            }else{
                $("#notif").html("Credentials not Found!");
                $("#notif").addClass("shake");
            }
        });
    });

    $("#adminLogin").on("submit", function(e){
        e.preventDefault();
        var url = "ajax/login_user.php";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            if(res == 1){
                window.location.replace("dashboard.php");
            }else{
                $("#notif").html("Credentials not Found!");
                $("#notif").addClass("shake");
            }
        });
    });

    // $("#login").click(function(e){

    //     var id = $("#student_id").val();
    //     var pw = $("#pw").val();

    //     e.preventDefault();
    //     $.ajax({
    //         url:"ajax/login_student.php",
    //         method:"POST",
    //         data:{
    //             id:id,
    //             pw:pw,
    //         },
    //         success: function(data){
    //             if(data == 1){
    //                 window.location.replace("dashboard.php");
    //             }else{
    //                 alert("You dont have the credential to access this site");
    //             }
    //         }
    //     });
    // });

  
</script>
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<!--// bottom-top -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

</body>
</html>