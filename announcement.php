<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Announcement</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          <div class="card" style="width: 100%;">
            <div class="card-header">
              <h3 class="card-title">Announcement
                <button type="button" class="btn btn-sm btn-primary pull-right" id="btn_trig" data-toggle="modal" data-target="#modal-default"><span class="fa fa-plus-circle"></span> Add Announcement</button></h3>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>What</th>
                  <th>When</th>
                  <th>Where</th>
                  <th>Whom</th>
                  <th>Other Info</th>
                  <th style="width: 15%;">Action</th>
                </tr>
                </thead>
                <tbody>

               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- add modal -->
      <form id="addAnn" action="" method="POST">
       <div class="modal fade" id="modal-default">
          <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add Announcement</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                
                <div class="input-group">
                  <span class="input-group-addon"><strong>What : </strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_what">
                </div> <br>

                <div class="input-group">
                  <span class="input-group-addon"><strong>Where:</strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_where">
                </div><br>

                 <div class="input-group date-time">
                  <span class="input-group-addon"><strong>When : </strong><span style="color:red;">*</span></span>
                   <input type="text" required class="form-control " id="datepicker_min" name="anc_when">
                </div><br>
                 <div class="input-group">
                  <span class="input-group-addon"><strong>Whom : </strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_whom">
                </div><br>
                 <div class="input-group">
                  <span class="input-group-addon"><strong>Info : </strong><span style="color:red;">*</span></span>
                  <textarea type="text" required class="form-control" name="other_info"> </textarea>
                </div><br>

              </div>
              <div class="modal-footer">
  
                <button type="button" class="btn btn-primary" id="btn_add">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
      <!-- end of add modal -->

      <!-- update announcement -->
       <form id="updateAnn" action="" method="POST">
      <div class="modal fade" id="update_ann">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Update Announcement</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                
              </div>
              <div class="modal-body">

                <input type="hidden" id="hidden_ann_id" name="hidden_ann_id" value="">
                
                <div class="input-group">
                  <span class="input-group-addon"><strong>What : </strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_what" id="update_anc_what">
                </div> <br>

                <div class="input-group">
                  <span class="input-group-addon"><strong>Where:</strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_where" id="update_anc_where">
                </div><br>

                 <div class="input-group">
                  <span class="input-group-addon"><strong>When : </strong><span style="color:red;">*</span></span>
                  <input type="text" required class="form-control" name="anc_when" id="datepicker_min1">
                </div><br>
                 <div class="input-group">
                  <span class="input-group-addon"><strong>Whom : </strong><span style="color:red;">*</span></span>
                  <input type="text" min="0" required class="form-control" name="anc_whom" id="update_anc_whom">
                </div><br>
                 <div class="input-group">
                  <span class="input-group-addon"><strong>Info : </strong><span style="color:red;">*</span></span>
                  <textarea type="text" required class="form-control" name="other_info" id="update_other_info"></textarea>
                </div><br>

              </div>
              <div class="modal-footer">
  
                <button type="button" class="btn btn-primary" id="btn_update">Save Changes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        </form>
        <!-- end update announcement -->

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
  <?php include "footer.php";?>

 
<script>

 function get_ann(){
  $('#example1').DataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/table_announcement.php",
      "dataSrc":"data"
    },
    "columns":[
      {
        "data":"count"
      },
 
      {
        "data":"anc_what"
      },
      {
        "data":"anc_where"
      },
      {
        "data":"date_sched"
      },
      {
        "data":"anc_whom"
      },
      {
        "data":"other_info"
      },
      {
        "mRender": function(data,type,row){
          if(row.anc_status == "1"){
              var display="btn-danger";
              var info = "Hide";
          }else{
            var display="btn-primary";
            var info = "Post";
          }

          return "<center><button class='btn "+display+" btn-sm' data-toggle='tooltip' title='POST' onclick='updatePost("+row.id+","+row.anc_status+")' id='btn_post"+row.id+"' value="+row.id+"'><span class='fa fa-send'></span> "+info+" </button> <button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Edit' onclick='getAnnouncement("+row.id+",\""+row.anc_what+"\",\""+row.anc_where+"\",\""+row.date_sched+"\",\""+row.anc_whom+"\",\""+row.other_info+"\")'><span class='fa fa-pencil'></span >Edit </button></center>";
        }
      }
    ]
  });
}

$('#btn_add').click(function(e){

  $("#btn_add").prop("disabled",true);
  $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");

  e.preventDefault();
  $.ajax({
    type:"POST",
    url:"ajax/addAnnouncement.php",
    data:$('#addAnn').serialize(),
    success:function(data){
      if(data == 1){
       success_add();
        $('#example1').DataTable().destroy();
        get_ann();
        $('#addAnn').each(function(){
          this.reset();
        });
      }else{
        failed_query();
      }
      $('#modal-default').modal("hide");
      $("#btn_add").prop("disabled",false);
      $("#btn_add").html("<span class='fa fa-check-circle'></span> Save Standard");
    }
  });
  
});

$('#btn_update').click(function(e){

  $("#btn_update").prop("disabled",true);
  $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");

  e.preventDefault();
  $.ajax({
    type:"POST",
    url:"ajax/update_Announcment.php",
    data:$('#updateAnn').serialize(),
    success:function(data){
      if(data == 1){
        // alert(data);
       success_update();
        $('#example1').DataTable().destroy();
        get_ann();
        $('#updateAnn').each(function(){
          this.reset();
        });
      }else{
         // alert(data);
        failed_query();
      }
      $('#update_ann').modal("hide");
      $("#btn_update").prop("disabled",false);
      $("#btn_update").html("<span class='fa fa-check-circle'></span> Save Standard");
    }
  });
  
});

function getAnnouncement(id,$anc_what,$anc_where,$date_sched,$anc_whom,$other_info){
  $("#hidden_ann_id").val(id);
  $("#update_anc_what").val($anc_what);
  $("#update_anc_where").val($anc_where);
  $("#update_anc_when").val($date_sched);
  $("#update_anc_whom").val($anc_whom);
  $("#update_other_info").val($other_info);
 
  $("#update_ann").modal("show");
}



function updatePost(id,status){

  $("#btn_post"+id).prop("disabled",true);
  $("#btn_post"+id).html("<span class='fa fa-spin fa-spinner'></span>");

  if(status==0){

    $.ajax({
    type:"POST",
    url:"ajax/update_ann.php",
    data:{
      id:id
    },
    success:function(data){
      if(data == 1){     
       success_update();
        $('#example1').DataTable().destroy();
        get_ann();  
      }else{
        failed_query();
      }
    }
  });

  }else{
    $.ajax({
    type:"POST",
    url:"ajax/update_ann_status.php",
    data:{
      id:id
    },
    success:function(data){
      if(data == 1){     
       success_update();
        $('#example1').DataTable().destroy();
         get_ann(); 
      }else{
        failed_query();
      }
    }
  });
 }
}

function changeAttr(){
   $("#update_anc_when").attr("type", "datetime-local");
}

$(document).ready(function(){
  get_ann();  

});
</script>