<?php
 include('core/config.php');


$sd = $_GET['sd'];
$u_type = $_GET['u_type'];

require_once 'dompdf/autoload.inc.php';
ob_start();
require_once('template/logs_template.php'); 
$template = ob_get_clean();

$html = $sd;

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


// $dompdf->loadHtml(file_get_contents('template/logs_template.php'));
$dompdf->loadHtml($template);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper(array(0,0,850,1600), 'Portrait');

// Render the HTML as PDF
$dompdf->render();

$file = $dompdf->output();


$date = date("F d, Y",strtotime($sd));
// Output the generated PDF to Browser
$dompdf->stream("Logs Report '".$date."'");



?>