<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h1>Announcement</h1>
          </div>
          <div class="col-sm-6">
            <center id="notif"> </center>
          </div>
          <div class="col-sm-3">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Announcement</li>

            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col -->
          
      <!--     <div class="col-md-4">
                <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                 <?php include "core/config.php";

              
                $max = mysqli_fetch_array($connectDB->query("SELECT max(event_date) FROM `tbl_event` ORDER BY `tbl_event`.`event_date` "));
                $min = mysqli_fetch_array($connectDB->query("SELECT min(event_date) FROM `tbl_event` ORDER BY `tbl_event`.`event_date` "));

                echo "<input type='hidden' id='max_date' value='$max[0]'>";
                 echo "<input type='hidden' id='min_date' value='$min[0]'>";
                ?>

                <input type="hidden" name="event_date" id="event_date_now" value="">
                <input type="hidden" name="event_id" id="event_id" value="">
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
                 <input type="hidden" name="date_now" id="date_now" value="<?php echo date("Y-m-d");?>">


                <h3 class="profile-username text-center"><span class="fa fa-arrow-left pull-left" id="btn_prev" style="color: #09509c;" onclick="prev()"> </span> Announcement  <span class="fa fa-arrow-right pull-right" style="color: #09509c;" id="btn_next" onclick="next()" > </span> </h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b> Name</b> <a class="float-right"> <span id="event_name"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Date </b> <a class="float-right"><span id="event_date"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Time </b> <a class="float-right"><span id="event_time"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Place</b> <a class="float-right"><span id="event_place"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Description</b> <a class="float-right"><span id="event_description"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact Person</b> <a class="float-right"><span id="contact_person"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact No.</b> <a class="float-right"><span id="contact_num"></span></a>
                  </li>
               
                </ul>
              </div>
            </div>
          </div> -->
      

          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-body p-0">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /. box -->
          </div>
          <!-- /.col -->
        </div>
        
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <strong>Copyright &copy; 2021 <strong>Manlucahoc Elementary School, Sipalay City, Negros Occidental</strong>.</strong>
    All rights reserved.
  </footer>


  <!-- /.control-sidebar -->
</div>
<!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->
<script src="moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->

<?php 
include "core/config.php";
$checkNumEventsSQL = $connectDB->query("SELECT * FROM tbl_event");
$numRowsEvents = mysqli_num_rows($checkNumEventsSQL);

?> 
<!-- Page specific script -->
<script>
  $(function () {


    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    $(document).ready(function() {
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      aspectRatio: 2.5,
     defaultDate: '<?php echo date('Y-m-d'); ?>',   // year-month-day
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      //Random default events
      events : [
          <?php 
          
        $ctrEvent=1;
        while($rowEvents = mysqli_fetch_array($checkNumEventsSQL)){
          $event_name = $rowEvents['event_name'];
          $start_date = $rowEvents['event_date'];
          $event_time = date("g:ia", strtotime($rowEvents['event_time']));
        ?>
        {
          title: '<?php echo $event_name." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          color: '#3498db',
          id: '<?php echo $rowEvents['event_id']; ?>',
          event_date: '<?php echo $rowEvents['event_date']; ?>',
          event_time: '<?php echo $rowEvents['event_time']; ?>',
          event_description: '<?php echo $rowEvents['event_description']; ?>',
          event_place: '<?php echo $rowEvents['event_place']; ?>',
          
          event_name: '<?php echo $rowEvents['event_name']; ?>' 
        }<?php 
          if($ctrEvent < $numRowsEvents){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
      ],

      eventClick: function(callEvent , jsEvent, view){
        $("#view_event").modal();
        $("#event_id").val(callEvent.id);
        $("#event_name").val(callEvent.event_name);
        $("#event_date").val(callEvent.event_date);
        $("#event_time").val(callEvent.event_time);
        $("#event_place").val(callEvent.event_place);
        $("#event_description").val(callEvent.event_description);

      },
      eventDragStop: function(event,jsEvent) {
        var event_id = event.id;
          var trashEl = jQuery('#calendarTrash');
          var ofs = trashEl.offset();


          var x1 = ofs.left;
          var x2 = ofs.left + trashEl.outerWidth(true);
          var y1 = ofs.top;
          var y2 = ofs.top + trashEl.outerHeight(true);

         
      },
      editable  : false,
      droppable : false, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })
  });

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      ini_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })


function next(){

  $("#btn_prev").css("color","green");
  $("#btn_prev").prop("disabled",false);


   var min_date=$("#min_date").val();
    var max_date=$("#max_date").val();
    var event_date_now=$("#event_date_now").val();

    if (event_date_now == max_date){
          $("#btn_next").prop("disabled",true);
          $("#btn_next").css("color","#abbfaa");

        }else{
          $.post("ajax/getNextEvent.php", {
                event_date_now: event_date_now
            },
            function (data, status) {
                var o = JSON.parse(data);
              
          $("#contact_person").html(o.contact_person);
          $("#contact_num").html(o.contact_num);
          $("#event_place").html(o.event_place);
          $("#event_description").html(o.event_description);
          $("#event_time").html(o.event_time);
          $("#event_date").html(o.event_date);
          $("#event_name").html(o.event_name);
          $("#event_date_now").val(o.event_date);
          $("#event_id").val(o.event_id);
          disabledNow();
          check_present(); 
         
           });
   }
   
}

function prev(){

  $("#btn_next").prop("disabled",false);
  $("#btn_next").css("color","green");

  var min_date=$("#min_date").val();
  var max_date=$("#max_date").val();
  var event_date_now=$("#event_date_now").val();

  if (event_date_now == min_date){
    $("#btn_prev").prop("disabled",true);
    $("#btn_prev").css("color","#abbfaa");
  }else{
    $.post("ajax/getPrevEvent.php", {
        event_date_now: event_date_now
      },
      function (data, status) {
          var o = JSON.parse(data);
        
        $("#contact_person").html(o.contact_person);
        $("#contact_num").html(o.contact_num);
        $("#event_place").html(o.event_place);
        $("#event_description").html(o.event_description);
        $("#event_time").html(o.event_time);
        $("#event_date").html(o.event_date);
        $("#event_name").html(o.event_name);
        $("#event_date_now").val(o.event_date);
        $("#event_id").val(o.event_id);
        disabledPrev();
        check_present();
     });
  }
}


function nowEvent(){
 
     $.post("ajax/getNowEvent.php", 
            function (data, status) {
                var o = JSON.parse(data);

              
          $("#contact_person").html(o.contact_person);
          $("#contact_num").html(o.contact_num);
          $("#event_place").html(o.event_place);
          $("#event_description").html(o.event_description);
          $("#event_time").html(o.event_time);
          $("#event_date").html(o.event_date);
          $("#event_name").html(o.event_name);
          $("#event_date_now").val(o.event_date);
          $("#event_id").val(o.event_id);

          disabledNow();
          check_present();
     });
}

function disabledNow(){
  var max_date=$("#max_date").val();
  var event_date_now=$("#event_date_now").val();

  if (max_date == event_date_now ){
   $("#btn_next").prop("disabled",true);
    $("#btn_next").css("color","#abbfaa");
  }
}

function disabledPrev(){
  var min_date=$("#min_date").val();
  var event_date_now=$("#event_date_now").val();

  if (min_date == event_date_now ){
   $("#btn_prev").prop("disabled",true);
    $("#btn_prev").css("color","#abbfaa");

   
  }
 
}


function present() {
  $("#notif").removeClass("animated fadeOut");

     var event_id = $("#event_id").val();
      $("#btn_present").prop('disabled', true);
      $("#btn_present").html("<span class='fa fa-spinner fa-spin'></span> Loading ...");

     $.ajax({
        type:"POST",
        url:"ajax/check_attendance_student.php",
        data:{
          event_id:event_id
        },
        success:function(data){


        if(data == 1){
          $("#notif").addClass("animated fadeIn");
          $("#notif").html("<span class='alert alert-success'> Successfully Saved. </span>");
        }else{
          $("#notif").addClass("animated fadeIn");
          $("#notif").html("<span class='alert alert-danger'> Sorry. Something went wrong. Please try again later. </span>");
        }
      $("#btn_present").prop('disabled', false);
      $("#btn_present").html("<span class='fa fa-check'></span> I'm Going");
        setTimeout(function(){
          $("#notif").removeClass("animated fadeIn");
        $("#notif").addClass("animated fadeOut");
         

       }, 2000);

        check_present();  
        }
      });
     
    }


function check_present() {


      var event_id = $("#event_id").val();
      var user_id = $("#user_id").val();

     $.ajax({
        type:"POST",
        url:"ajax/check_present.php",
        data:{
          event_id:event_id,
          user_id:user_id
        },
        success:function(data){
        if(data == 0){
          var event_date_now = $("#event_date_now").val();
          var date_now = $("#date_now").val();

            if(date_now <= event_date_now){
              $("#list_btn").css("display","block");
            }else{
              $("#list_btn").css("display","none");
            }
        }else{
          $("#list_btn").css("display","none");
        }
        }
      });
     
    }

    $(document).ready(function (){
        nowEvent();
    });

</script>
</body>
</html>
