<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Students</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class='col-sm-4 input-group' style="margin-top: 10px">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong> Year : <span style="color:red;">*</span></span></strong>
                    </div>
                      <select class='form-control' id="sy" style="text-transform: capitalize;margin-right: 10px;" onchange="getSection()">
                        <option value="">-- Select Year --</option>
                        <?php 

                        include "core/config.php";

                        $event = $connectDB->query("SELECT * from tbl_student group by year ");
                      while($row = mysqli_fetch_array($event)){ ?>
                                <option value="<?php echo $row['year'];?>"><?php echo $row['year'] ?></option>

                        <?php } ?>
                      </select>
                </div>
                <div class='col-sm-4 input-group' style="margin-top: 10px">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong> Section: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                   <select class='form-control' id="section" style="text-transform: capitalize;margin-right: 10px;">
                     <option value="">-- Select Year --</option>
                    </select>
                </div>
                <div class='col-sm-4 input-group' style="margin-top: 10px; padding: 2px;">
                    <button class="btn btn-primary btn-sm" onclick="gen()" id="btn_gen"><span class="fa fa-refresh"></span> Generate </button>
                     <button class="btn btn-success btn-sm" onclick="showUploadModal()"><span class="fa fa-upload"> </span> Upload File </button>
                    <button class="btn btn-warning btn-sm" id="btn_download"><span class="fa fa-download"> </span> Download Template </button>
                    <?php require 'modals/modal_upload.php'; ?>
                </div>
              </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <!-- /.container-fluid -->
    </section>
   <?php require 'modals/modal_response.php'; ?>
     <!-- Main content -->
    <section class="content" id="show" style="display: none;">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="    padding: 20px 20px 0px;">
              <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Add Student </button>
              <button class="btn btn-danger btn-sm" onclick="deleteStudent()" id='btn_delete'><span class="fa fa-trash-o"> </span> Delete Student </button>
          </div>
          
          <?php require 'modals/modal_add_student.php';?>
          <?php require 'modals/modal_update_student.php'; ?>
           <?php require 'modals/modal_view_student.php'; ?>
            <?php require 'modals/modal_view_attendance.php'; ?>
          
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><center><input type="checkbox" onchange="checkAll(this)"></center></th>
                  <th>Student ID</th>
                  <th>Name</th>
                  <th>Year</th>
                  <th>Section</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
    
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>
  <script src="plugins/highchart/highcharts.js"></script>
<script src="plugins/highchart/highcharts-drilldown.js"></script>
<script src="plugins/highchart/exporting.js"></script>
  <script type="text/javascript">
    
$("#btn_download").click(function(e){
  e.preventDefault();
  var dl = confirm("Download Template for Student(csv file).");
  if(dl == true){
    window.location = 'ajax/download_template.php?bid=';
  }
  
});

    function showUploadModal(){
  $("#modalStudent").modal('show');
}

function showAddModal(){
  $("#modalAddStudent").modal('show');
}

function getSection(){
  var sy = $("#sy").val();
   $.post("ajax/getSection.php", {
                sy: sy
            },
            function (data) {
              $("#section").html(data);
     });
}

function graphical_view(id){

$("#chart").html("<h4><center><span class='fa fa-spinner fa-spin'></span> Loading graph ...</center></h4>");

$.getJSON('ajax/student_chart.php?student_id='+id+'', function(data){
  //var json_data = JSON.parse(data);
  // Create the chart
  Highcharts.chart("chart", {
    chart: {
      type: 'column'
    },
    title: {
      text: "Graphical View for student assessment"
    },
    xAxis: {
      type: 'category',
      title: {
        // text: 'Laundry Merchant'
      }
    },
    yAxis: {
      title: {
        text: 'Average Score'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">{point.name} </span> <span style="font-size:11px">{series.name}</span>: <b>{point.y:.0f}</b><br/>'
    },

   series: data['series'],
  });
});
}


function update(id){
        $.post("ajax/getStudent.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
          $("#update_stud_id").val(o.student_id);
          $("#update_fname").val(o.fname);
          $("#update_lname").val(o.lname);
          $("#update_year").val(o.year);
          $("#update_section").val(o.section);
          $("#update_id").val(o.stud_id);
     });
  $("#modalUpdateStudent").modal("show");
}

function viewProf(id){
        $.post("ajax/getStudent.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
                var teacher_id = o.t_id;


                 load(teacher_id);
          $("#view_stud_id").val(o.stud_id);
          $("#view_fname").val(o.fname);
          $("#view_lname").val(o.lname);
          $("#view_year").val(o.year);
          $("#view_section").val(o.section);
          $("#view_add").val(o.address);
          $("#view_contact").val(o.contact);
          $("#view_email").val(o.email);
          $("#view_id").val(o.user_id);
     });
  $("#modalViewStudent").modal("show");
}

function load(id){
  $.post("ajax/getFaculty.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
          $("#view_teacher").val(o.t_fname +" "+o.t_lname);
     });
}
function viewAtt(id){
  $("#modalViewAttendance").modal("show");

  table_view(id);
  graphical_view(id);
}

function table_view(stud_id){
  var table = $('#view_attendance').DataTable();
  table.destroy();
  $("#view_attendance").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/table_view_score.php",
      "dataSrc":"data",
      "data":{stud_id:stud_id}
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"subject"
      },
      {
        "data":"category"
      },
      {
        "data":"score"
      },
      {
        "data":"date_added"
      }
    ]
  });
}
$("#upload_student").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_template.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      $("#modalStudent").modal("hide");
      $("#modalResponse").modal('show');
      $("#response").html(data);
      
      get_Student();
    }
  });
});


  $("#add_student").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_student.php",
      method:"POST",
      data:$("#add_student").serialize(),
      success: function(data){

        if(data == 1){
          get_Student();
         success_add();
         $("#add_student")[0].reset();

        }else if(data == 2){
          exist();
        }else if(data == 3){
          no_teacher();
        }else{

         failed_query();
        }
        $("#modalAddStudent").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_student").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_student.php",
      method:"POST",
      data:$("#update_student").serialize(),
      success: function(data){
          if(data == 1){
          get_Student();
         success_update();
        }else if(data == 2){
          exist();
        }else if(data == 3){
          no_teacher();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Update ");
      }
    });
  });

  function deleteStudent(){

   var checkedValues =  $("input[name='delete_student']:checked").map(function(){
    return this.value;
  }).get();
    
  id = [];
  if(checkedValues == ""){
    alertMe("Aw Snap!","No selected Student","warning");
  }else{
    var retVal = confirm("Are you sure you want to delete?");
    if( retVal == true ){
      $("#btn_delete").prop("disabled", true);
      $("#btn_delete").html("<span class='fa fa-spin fa-spinner'></span> Loading");
      $.post("ajax/delete_student.php", {
        id: checkedValues
      },
      function (data, status) {
        
      $("#response").html(data);
      $("#modalResponse").modal('show');
        get_Student();
      $("#btn_delete").prop("disabled", false);
      $("#btn_delete").html("<span class='fa fa-trash-o'></span> Delete Student");
      }

    );
    }

  }
  
}


  function get_Student(){

  var sy = $("#sy").val();
  var section = $("#section").val();
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/student_dt.php",
      "dataSrc":"data",
      "data":{
          sy:sy,
          section:section
        }
    },
    "columns":[
      {
        "mRender": function(data,type,row){

          if (row.allow==1){
              return "<center><input type='checkbox' value='" + row.id+ "' name='delete_student'></center>";
          }else{
              return "";
          }
          

        }
      },
      {
        "data":"stud_id"
      },
      {
        "data":"name"
      },
      {
        "data":"year"
      },
      {
        "data":"section"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Update Record' value='" + row.id+ "' onclick='update(" + row.id+")'><span class='fa fa-pencil'></span></button><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Record' value='" + row.id+ "' onclick='viewProf(" + row.id+")'><span class='fa fa-eye'></span></button><button class='btn btn-default btn-sm' data-toggle='tooltip' title='View Score' value='" + row.id+ "' onclick='viewAtt(" + row.id+")'><span class='fa fa-star'></span></button></center>";
        }
      }
    ]
  });
}

 function gen() {
        var sy = $("#sy").val();
        var section = $("#section").val();

    if (sy == "" ||  section == ""){
      alert ("Please fill in the form");
    }else{
        $("#show").css("display","block");
        get_Student();
     } 
}
  

</script>