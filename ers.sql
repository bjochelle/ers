-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2021 at 01:01 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ers`
--

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `audio_id` int(11) NOT NULL,
  `audio_file` text NOT NULL,
  `date_added` datetime NOT NULL,
  `stud_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`audio_id`, `audio_file`, `date_added`, `stud_id`, `quiz_id`, `score`) VALUES
(8, '20210524-130127_105.mp3', '2021-05-24 07:01:27', 29, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_file` text NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL,
  `module_name` text NOT NULL,
  `module_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_file`, `subject_id`, `date_added`, `user_id`, `level`, `module_name`, `module_data`) VALUES
(1, 'test.doc', 1, '2021-05-22 00:00:00', 28, 'easy', 'Test module ', 'The Philippines is bounded by the South China Sea to the west, the Philippine Sea to the </br>east, and the Celebes Sea to the southwest. and shares maritime borders with Taiwan to the north, Japan to the northeast, Palau to the east and southeast, Indonesia to the south, Malaysia and Brunei to the southwest, Vietnam to the west, and China to the northwest. The Philippines covers an area of 300,000 km2 (120,000 sq mi) and, as of 2020, had a population of around 109 million people, making it the world\'s twelfth-most populous country. The Philippines is a multinational state, with diverse ethnicities and cultures throughout its islands. Manila is the nation\'s capital, while the largest city is Quezon City, both lying within the urban area of Metro Manila.'),
(2, '', 1, '2021-05-23 00:00:00', 2, 'hard', 'update module', '  module test update '),
(3, '', 2, '2021-05-23 00:00:00', 2, 'medium', 'dasd', 'sdasdsadsa'),
(4, '', 2, '2021-05-23 00:00:00', 2, 'easy', 'dsadsa', 'sadsadsadas'),
(5, '', 2, '2021-05-24 00:00:00', 28, 'medium', 'module ni qq', 'module ni qq');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_details`
--

CREATE TABLE `quiz_details` (
  `quiz_dtl_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `questions` int(11) NOT NULL,
  `answer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_header`
--

CREATE TABLE `quiz_header` (
  `quiz_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `quiz_name` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL,
  `quiz_data` text NOT NULL,
  `date_added` datetime NOT NULL,
  `deadline` date NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_header`
--

INSERT INTO `quiz_header` (`quiz_id`, `subject_id`, `quiz_name`, `user_id`, `level`, `quiz_data`, `date_added`, `deadline`, `status`) VALUES
(1, 2, 'medium', 2, '', ' Quiz info here', '2021-05-23 00:00:00', '0000-00-00', 0),
(2, 3, 'test quiz', 29, 'easy', 'Buffet. Valet. The Philippines is bounded by the South China Sea to the west, the Philippine Sea to the </br>east, and the Celebes Sea to the southwest. and shares maritime borders with Taiwan to the north, Japan to the northeast, Palau to the east and southeast, Indonesia to the south, Malaysia and Brunei to the southwest, Vietnam to the west, and China to the northwest. The Philippines covers an area of 300,000 km2 (120,000 sq mi) and, as of 2020, had a population of around 109 million people, making it the world\'s twelfth-most populous country. The Philippines is a multinational state, with diverse ethnicities and cultures throughout its islands. Manila is the nation\'s capital, while the largest city is Quezon City, both lying within the urban area of Metro Manila.', '2021-05-23 00:00:00', '0000-00-00', 0),
(3, 3, 'test quiz update', 2, 'hard', ' Sample quiz here update\r\n ', '2021-05-23 00:00:00', '0000-00-00', 1),
(4, 3, 'test', 28, 'medium', ' Quiz info hereadsasdasds', '2021-05-24 00:00:00', '0000-00-00', 1),
(5, 2, 'asdasdasd', 29, 'hard', '  Quiz info hereasdasdasdas ', '2021-05-24 00:00:00', '2021-06-01', 1),
(6, 2, 'Sample Quiz', 28, 'medium', 'quiz ni qqqqqq', '2021-05-24 00:00:00', '2021-06-05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `score_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `subject_code` text NOT NULL,
  `subject_name` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `subject_code`, `subject_name`, `date_added`) VALUES
(1, '21312', 'dasd', '2021-05-22 00:00:00'),
(2, '234214', 'asdsad', '2021-05-22 00:00:00'),
(3, 'math1', 'Mathematics 1', '2021-05-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_description` varchar(100) NOT NULL,
  `event_place` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `contact_person` varchar(225) NOT NULL,
  `contact_num` varchar(225) NOT NULL,
  `qr_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`event_id`, `event_name`, `event_date`, `event_time`, `event_description`, `event_place`, `date`, `contact_person`, `contact_num`, `qr_code`) VALUES
(3, 'alumni  2018', '2021-05-29', '11:57:00', 'Alumni 2018', 'Fortune ', '0000-00-00', '', '', ''),
(6, 'beach Volley', '2020-11-30', '22:58:00', 'Summer time ', 'Boracay ', '0000-00-00', '', '', ''),
(7, 'Alumni', '2020-09-19', '20:00:00', 'Yearly alumni host by batch 1975', 'Business Inn', '0000-00-00', '', '', ''),
(8, 'Speaking Event With a Famous Alumni', '2020-12-24', '19:30:00', 'Better way to attend the alumni ', 'Mansilingan ', '0000-00-00', '', '', ''),
(9, ' Music Festival', '2020-10-18', '21:15:00', 'Music is life', 'California ', '0000-00-00', '', '', ''),
(10, 'Golf Tournament', '2020-11-25', '18:00:00', 'Enjoy and have fun ', 'Pannad ', '0000-00-00', '', '', ''),
(11, 'Sports Event', '2020-08-23', '09:00:00', 'Play it ', 'Panaad ', '0000-00-00', '', '', ''),
(13, 'Friendly Competition ', '2020-11-10', '20:40:00', 'Friendly night', 'Alijis Gym ', '0000-00-00', '', '', ''),
(14, 'Get Bowled Over', '2020-06-13', '13:30:00', 'play it', 'Malaysia ', '0000-00-00', '', '', ''),
(15, 'dasdas', '2020-05-08', '13:02:00', 'fdaskhh', 'hkhjk', '0000-00-00', 'khhkhk', '42374687', ''),
(16, 'sadasd', '2020-05-09', '14:02:00', 'dsfjhjh', 'hjhj', '0000-00-00', 'hkhjhk', '35246', ''),
(30, '42141', '2021-02-02', '14:01:00', 'dsadsa', 'dasd', '0000-00-00', 'sadsa', '41234', '2acceba4717d8663af037384c37384fa'),
(32, 'Special Event', '2020-08-25', '23:06:00', 'test special Event', 'sm', '0000-00-00', 'Sam Will', '09123456789', '39ab90f12d72bc60c732f2e9d86b439a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notif`
--

CREATE TABLE `tbl_notif` (
  `notif_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `notif_status` varchar(1) NOT NULL,
  `view_status` int(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notif`
--

INSERT INTO `tbl_notif` (`notif_id`, `remarks`, `notif_status`, `view_status`, `date_added`) VALUES
(1, 'chen is newly Registered', 'R', 1, '2020-05-08 20:26:17'),
(2, 'Christian Sad interested in the event of dasdas', 'A', 1, '2020-04-29 16:31:50'),
(3, 'Christian Sad interested in the event of Get Bowled Over', 'A', 1, '2020-05-13 14:14:34'),
(4, 'Christian Sad interested in the event of Alumni night out ', 'A', 1, '2020-05-13 14:16:00'),
(5, 'Christian Sad interested in the event of dasd', 'A', 1, '2020-05-13 14:22:36'),
(6, 'John Doe interested in the event of Alumni', 'A', 0, '2020-08-25 10:31:24'),
(7, 'sad asd interested in the event of Special Event', 'A', 0, '2020-08-25 11:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `stud_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date_added` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` int(11) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `ishidden` int(1) NOT NULL,
  `student_id` varchar(225) NOT NULL,
  `pw` varchar(225) NOT NULL,
  `year` varchar(225) NOT NULL,
  `section` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`stud_id`, `t_id`, `filename`, `fname`, `lname`, `email`, `date_added`, `address`, `contact`, `bday`, `contact_number`, `ishidden`, `student_id`, `pw`, `year`, `section`) VALUES
(29, 22, '', 'QQQ', 'QQ', '', '2021-05-21', '', 0, '0000-00-00', '', 0, '1', '12345', '1', '1'),
(30, 21, '', 'WWWW', 'RR', '', '2021-05-21', '', 0, '0000-00-00', '', 0, '2', '12345', '1', '2'),
(33, 22, '', 'dasdsad', 'hey', '', '2021-05-21', '', 0, '0000-00-00', '', 0, '3312321', '12345', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `t_id` int(11) NOT NULL,
  `t_fname` varchar(225) NOT NULL,
  `t_lname` varchar(225) NOT NULL,
  `t_year` varchar(225) NOT NULL,
  `t_section` varchar(225) NOT NULL,
  `date_added` date NOT NULL,
  `status` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`t_id`, `t_fname`, `t_lname`, `t_year`, `t_section`, `date_added`, `status`) VALUES
(21, 'QQQ', 'QQQ', '1', '2', '2021-05-21', ''),
(22, 'WWWW', 'WWW', '1', '1', '2021-05-21', ''),
(23, 'vcxvcxv', 'c', '3', '2', '2021-05-21', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(1) NOT NULL,
  `date_added` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `un` varchar(100) NOT NULL,
  `pw` varchar(100) NOT NULL,
  `contact` int(11) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `ishidden` int(1) NOT NULL,
  `t_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `filename`, `fname`, `lname`, `email`, `user_type`, `date_added`, `address`, `un`, `pw`, `contact`, `bday`, `contact_number`, `ishidden`, `t_id`) VALUES
(2, 'jeongyeon-twice-uhdpaper.com-4K-5.642-wp.thumbnail.jpg', 'Test', 'Admin', 'admin@gmail.com', 'A', '2018-08-29', 'Alijis', 'a', 'a', 2147483647, '0000-00-00', '', 0, 0),
(28, '', 'QQQ', 'QQQ', '', 'T', '2021-05-21', '', 'QQQ12', '12345', 0, '0000-00-00', '', 0, 21),
(29, '', 'WWWW', 'WWW', '', 'T', '2021-05-21', '', 'WWW11', '12345', 0, '0000-00-00', '', 0, 22),
(30, '', 'vcxvcxv', 'c', '', 'T', '2021-05-21', '', 'c', '12345', 0, '0000-00-00', '', 0, 23);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`audio_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `quiz_details`
--
ALTER TABLE `quiz_details`
  ADD PRIMARY KEY (`quiz_dtl_id`);

--
-- Indexes for table `quiz_header`
--
ALTER TABLE `quiz_header`
  ADD PRIMARY KEY (`quiz_id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`score_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`stud_id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `audio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quiz_details`
--
ALTER TABLE `quiz_details`
  MODIFY `quiz_dtl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quiz_header`
--
ALTER TABLE `quiz_header`
  MODIFY `quiz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `stud_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
