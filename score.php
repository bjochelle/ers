<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Score</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="padding: 20px;">
            
          </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Quiz Name</th>
                   <th>Subject</th>
               <!--    <th>Level</th>
                  <th>Subject</th>
                  <th>Teacher</th>
                  <th>Score</th> -->
                  <th style="width: 25% !important;">Status</th>
    <!--               <th>Date Added</th> -->
                  <th>File</th>
                   <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_view_score.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    
    function view(id){
        $.post("ajax/getScore.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);

                console.log(o)
          $("#quiz_name").html(o.quiz_name);
          $("#subject").html(o.subject);
          $("#level").html(o.level);
          $("#teacher").html(o.teacher);
          $("#date_added").html(o.date_added);
     });
  $("#modalViewScore").modal("show");
}

  function get_Faculty(){
  var table = $('#example1').DataTable();
  table.destroy();

  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/score_dt.php",
      "dataSrc":"data",
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"quiz_name"
      },
      {
        "data":"subject"
      },
      // {
      //   "data":"level"
      // },
    
      // {
      //   "data":"teacher"
      // },
      // {
      //   "data":"score"
      // },
      {
        "data":"status"
      },
      // {
      //   "data":"date_added"
      // },
      {
        "mRender": function(data,type,row){
          if(row['allow'] === 0){
            var read = "";
          }else{
            var voice = '<audio class="av-recorder-audio" controls="" style="" src="ajax/uploads/'+row.file+'"></audio>';
        
          }

          return "<center>"+voice+"</center>";
        }
      },
       {
        "mRender": function(data,type,row){
      
          return "<center><button id='closeopen_btn"+row.id+"' class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='" + row.id+ "' onclick='view("+row.id+")'><span class='fa fa-eye'></span> View Details </button></center>";
        }
      }
    ]
  });
}
  
$(document).ready(function (){
  get_Faculty();
});
</script>