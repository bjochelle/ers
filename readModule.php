<?php 
include "core/config.php";
$checkNumEventsSQL = $connectDB->query("SELECT * FROM modules where module_id='$_GET[id]'");
$rowEvents = mysqli_fetch_array($checkNumEventsSQL);

$subject = mysqli_fetch_array($connectDB->query("SELECT * FROM subjects where subject_id='$rowEvents[subject_id]'"));
$teacher = mysqli_fetch_array($connectDB->query("SELECT * FROM tbl_user where user_id='$rowEvents[user_id]'"));

if($rowEvents['module_file'] != ''){
 

  $file = $rowEvents['module_file'];
}else{
  $file = "";
}

?> 
<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Read PHIL-IRI</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
  <div class="card-body">
  <div class="form-row align-items-center">


    <div class="col-md-4">
      <h5 class="input-group mb-2">
          Subject : <?=$subject['subject_name'];?> (<?=$subject['subject_code'];?>) 
      </h5>
    </div>

      <div class="col-md-4">
      <h5 class="input-group mb-2">
          Level :  <?=$rowEvents['level'];?> 
      </h5>
    </div>


  <div class="col-md-4">
      <h5 class="input-group mb-2">
          Teacher :  <?=$teacher['fname'];?> <?=$teacher['lname'];?> 
      </h5>
    </div>



<div class='col-sm-6' >
<div class="card container" style="height:500px;overflow: auto;">
<div class="articulate-area">
  <button onclick="speak('article')" class="btn btn-primary">Speak All</button>
  <button onclick="pause()" class="btn btn-danger">Pause</button>
  <button onclick="resume()" class="btn btn-info">Resume</button>
  <button onclick="stop()" class="btn btn-success">Stop</button>
</div>
<hr>

  <div class="card-body">
    <h5 class="card-title" style="text-align: center;"> <?=$rowEvents['module_name'];?> </h5><br>
    <article>
    <p style="white-space: pre-wrap;"><?=$rowEvents['module_data'];?></p>
    </article>
 
  </div>
<!--   <div class="card-footer text-muted">
      <a href="#">Download</a>
  </div> -->
</div>

</div>
<div class='col-sm-6' >
<div class="card container" style="height:500px;overflow: auto;">

<hr>

  <div class="card-body">
  <?php if($file == ""){ ?>
    <h4 style='text-align:center;color:red' >NO MODULE FILE ATTACHED</h4>-->
<?php }else { ?>
  <button onclick="textToAudio()" class='btn btn-sm btn-primary btn-round'>Click Me! To Read Content</button>
  <div id="output"><?=$rowEvents['file_content']?></div>
  
<?php }  ?>
 
  </div>

</div>
</div>


</div>

        </div>
      </div>
    </div>
  </section>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="dist/js/articulate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.10.100/pdf.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.6.347/pdf.worker.entry.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.10.100/pdf.worker.min.js" ></script>
<script>
  $(document).ready( function(){
    ExtractText();
  });
   function textToAudio() {
        var msg = $("#output").text();
        var speech = new SpeechSynthesisUtterance();
        speech.lang = "en-US";
        
        speech.text = msg;
        speech.volume = 1;
        speech.rate = 1;
        speech.pitch = 1;
        
        window.speechSynthesis.speak(speech);
    }
var datass = '';
        var DataArr = [];
        PDFJS.workerSrc = '';

        function ExtractText() {
            var input = document.getElementById("pdfFile");
            var fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            console.log(input.files[0]);
            fReader.onloadend = function (event) {
                convertDataURIToBinary(event.target.result);
            }
        }

        var BASE64_MARKER = ';base64,';

        function convertDataURIToBinary(dataURI) {

            var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = dataURI.substring(base64Index);
            var raw = window.atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(new ArrayBuffer(rawLength));

            for (var i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            pdfAsArray(array)

        }

        function getPageText(pageNum, PDFDocumentInstance) {
            // Return a Promise that is solved once the text of the page is retrieven
            return new Promise(function (resolve, reject) {
                PDFDocumentInstance.getPage(pageNum).then(function (pdfPage) {
                    // The main trick to obtain the text of the PDF page, use the getTextContent method
                    pdfPage.getTextContent().then(function (textContent) {
                        var textItems = textContent.items;
                        var finalString = "";

                        // Concatenate the string of the item to the final string
                        for (var i = 0; i < textItems.length; i++) {
                            var item = textItems[i];

                            finalString += item.str + " ";
                        }

                        // Solve promise with the text retrieven from the page
                        resolve(finalString);
                    });
                });
            });
        }

        function pdfAsArray(pdfAsArray) {

            PDFJS.getDocument(pdfAsArray).then(function (pdf) {

                var pdfDocument = pdf;
                // Create an array that will contain our promises
                var pagesPromises = [];

                for (var i = 0; i < pdf.pdfInfo.numPages; i++) {
                    // Required to prevent that i is always the total of pages
                    (function (pageNumber) {
                        // Store the promise of getPageText that returns the text of a page
                        pagesPromises.push(getPageText(pageNumber, pdfDocument));
                    })(i + 1);
                }

                // Execute all the promises
                Promise.all(pagesPromises).then(function (pagesText) {

                    // Display text of all the pages in the console
                    // e.g ["Text content page 1", "Text content page 2", "Text content page 3" ... ]
                    console.log(pagesText); // representing every single page of PDF Document by array indexing
                    console.log(pagesText.length);
                    var outputStr = "";
                    for (var pageNum = 0; pageNum < pagesText.length; pageNum++) {
                        console.log(pagesText[pageNum]);
                        outputStr = "";
                        outputStr = "<br/><br/>Page " + (pageNum + 1) + " contents <br/> <br/>";

                        var div = document.getElementById('output');

                        div.innerHTML += (outputStr + pagesText[pageNum]);

                    }




                });

            }, function (reason) {
                // PDF loading error
                console.error(reason);
            });
        }
function speak(obj) {
  $(obj).articulate('speak');
};

function pause() {
  $().articulate('pause');
};

function resume() {
  $().articulate('resume');
};

function stop() {
  $().articulate('stop');
};
</script>

</body>
</html>
