<?php include "header.php";
$id = $_GET['id'];

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Takers</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Student Name</th>
                  <th>Exercise Name</th>
                  <th>Level</th>
                  <th>Subject</th>
                  <th>Deadline</th>
                  <th>File</th>
                  <th>Score</th>
                  <th style="width: 20%;">Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/add_score_modal.php'; ?>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

<script type="text/javascript">
$("#add_score").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
    url:"ajax/add_score.php",
    method:"POST",
    data:$("#add_score").serialize(),
    success: function(data){
        if(data == 1){
        get_Faculty();
            success_add();
        }else if(data == 2){
            failed_query();
        }else{
            failed_query();
        }
        $("#addScore").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
    }
    });
});
function addScore(id){
    $("#addScore").modal();
    $("#audioID").val(id);
}
function get_Faculty(){
var table = $('#example1').DataTable();
table.destroy();

var user_type = "<?php echo $user_type; ?>";
var quiz_id = "<?=$id?>";
$("#example1").dataTable({
"processing":true,
"ajax":{
    "url":"ajax/datatables/quiz_takers_dt.php",
    "dataSrc":"data",
    "type":"POST",
    "data":{
        user_type:user_type,
        quiz_id: quiz_id
    }
},
"columns":[
    {
    "data":"count"
    },
    {
    "data":"student"
    },
    {
    "data":"quiz_name"
    },
    {
    "data":"level"
    },
    {
    "data":"subject"
    },
    {
    "data":"deadline"
    },
    {
    "mRender": function(data,type,row){
        if(row.allow == 0){
            var voice = "";
        }else{
            var voice = '<audio class="av-recorder-audio" controls="" style="" src="ajax/uploads/'+row.file+'"></audio>';
     
        }

        return "<center>"+voice+"</center>";
    }
    },
    {
    "data":"score_value"
    },
    {
    "mRender": function(data,type,row){
        if((row.score == 0 || row.score == null) && row.userType != 'S'){
            var viewQuiztakers = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Add Score' onclick='addScore("+row.id+")'><span class='fa fa-plus-circle'></span> Add Score</button>";
        }else{
            var viewQuiztakers = "";
        }
        return "<center>"+viewQuiztakers+"</center>";
    }
    }
]
});
}
  
$(document).ready(function (){
  get_Faculty();
});
</script>