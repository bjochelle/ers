<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Phil - IRI</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
     <form id="add_module">
  <div class="form-row align-items-center">

    <div class="col-sm-4" style='margin-top: 10px'>
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Title</div>
        </div>
        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Title" name="module_name">
      </div>
    </div>

    <div class="col-sm-4" style='margin-top: 10px'>
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Subject</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="subject_id">
        <option selected>Choose...</option>
        <!-- Page specific script -->

        <?php 
        include "core/config.php";
        $checkNumEventsSQL = $connectDB->query("SELECT * FROM subjects");
         while($rowEvents = mysqli_fetch_array($checkNumEventsSQL)){
        ?> 
        <option value="<?=$rowEvents['subject_id']?>"><?=$rowEvents['subject_name']?> (<?=$rowEvents['subject_code']?>)</option>
          <?php }?>
      </select>
      </div>
    </div>

     <div class="col-sm-4" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Level</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="level">
        <option selected>Choose...</option>
        <option value="easy">Easy</option>
        <option value="medium">Medium</option>
        <option value="hard">Hard</option>
      </select>
      </div>
    </div>

    <div class="col-sm-6" style='margin-top: 10px'>
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Module File</div>
        </div>
        <input onchange="ExtractText()" type="file" class="form-control" accept=".pdf" aria-describedby="basic-addon1" name="pdfFile[]" multiple="multiple" id='pdfFile' required/>
      </div>
    </div>

    <div class="col-md-12" style='margin-top: 10px'>
    <textarea class='form-control' rows="10" cols="125" name="module_data"> Module info here</textarea>
  </div>

  <textarea style="display: none"  rows="10" cols="125" name="output" id='output'></textarea>

 
    <div class="col-sm-12" style='margin-top: 10px'>
      <button type="submit" class="btn btn-primary pull-right" id="btn_add">Create</button>
    </div>
  </div>
</form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.10.100/pdf.min.js" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.6.347/pdf.worker.entry.min.js" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.10.100/pdf.worker.min.js" ></script>
  <?php include "footer.php";?>
  
  <script type="text/javascript">
  var datass = '';
        var DataArr = [];
        PDFJS.workerSrc = '';

        function ExtractText() {
            var input = document.getElementById("pdfFile");
            var fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            console.log(input.files[0]);
            fReader.onloadend = function (event) {
                convertDataURIToBinary(event.target.result);
            }
        }

        var BASE64_MARKER = ';base64,';

        function convertDataURIToBinary(dataURI) {

            var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = dataURI.substring(base64Index);
            var raw = window.atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(new ArrayBuffer(rawLength));

            for (var i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            pdfAsArray(array)

        }

        function getPageText(pageNum, PDFDocumentInstance) {
            // Return a Promise that is solved once the text of the page is retrieven
            return new Promise(function (resolve, reject) {
                PDFDocumentInstance.getPage(pageNum).then(function (pdfPage) {
                    // The main trick to obtain the text of the PDF page, use the getTextContent method
                    pdfPage.getTextContent().then(function (textContent) {
                        var textItems = textContent.items;
                        var finalString = "";

                        // Concatenate the string of the item to the final string
                        for (var i = 0; i < textItems.length; i++) {
                            var item = textItems[i];

                            finalString += item.str + " ";
                        }

                        // Solve promise with the text retrieven from the page
                        resolve(finalString);
                    });
                });
            });
        }

        function pdfAsArray(pdfAsArray) {

            PDFJS.getDocument(pdfAsArray).then(function (pdf) {

                var pdfDocument = pdf;
                // Create an array that will contain our promises
                var pagesPromises = [];

                for (var i = 0; i < pdf.pdfInfo.numPages; i++) {
                    // Required to prevent that i is always the total of pages
                    (function (pageNumber) {
                        // Store the promise of getPageText that returns the text of a page
                        pagesPromises.push(getPageText(pageNumber, pdfDocument));
                    })(i + 1);
                }

                // Execute all the promises
                Promise.all(pagesPromises).then(function (pagesText) {

                    // Display text of all the pages in the console
                    // e.g ["Text content page 1", "Text content page 2", "Text content page 3" ... ]
                    console.log(pagesText); // representing every single page of PDF Document by array indexing
                    console.log(pagesText.length);
                    var outputStr = "";
                    for (var pageNum = 0; pageNum < pagesText.length; pageNum++) {
                        console.log(pagesText[pageNum]);
                        outputStr = "";
                        outputStr = "<br/><br/>Page " + (pageNum + 1) + " contents <br/> <br/>";

                        var div = document.getElementById('output');

                        div.innerHTML += (outputStr + pagesText[pageNum]);

                    }




                });

            }, function (reason) {
                // PDF loading error
                console.error(reason);
            });
        }
    $("#add_module").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
      url:"ajax/add_module.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false, 
      cache: false,
      processData:false,
      success: function(data)
        {
          if(data == 1){
         success_add();
        setTimeout(function(){
           window.location.href = "modules.php";
         },1500)

        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
        }
               
     });
  }));

  // $("#add_module").submit(function(e){

  //   $("#btn_add").prop("disabled",true);
  //   $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
  //   e.preventDefault();
  //   $.ajax({
  //     url:"ajax/add_module.php",
  //     method:"POST",
  //     data:$("#add_module").serialize(),
  //     success: function(data){
  //       if(data == 1){
  //        success_add();
  //       setTimeout(function(){
  //          window.location.href = "modules.php";
  //        },1500)

  //       }else if(data == 2){
  //         failed_query();
  //       }else{
  //        failed_query();
  //       }
  //       $("#btn_add").prop("disabled",false);
  //       $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
  //     }
  //   });
  // });

</script>