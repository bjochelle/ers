<style type="text/css">
  #tab .nav-link.active, .nav-pills .show>.nav-link{
    color: #fff;
    background-color: #0f7b0a !important;
  }
  #tab .nav-link:not(.active):hover {
    color: #0f7b0a !important;
}
</style>
<?php include "header.php";
  include "core/config.php";

  $t_id = $_GET['t_id'];

  $f = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_teachers` where t_id='$t_id'"));
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-success card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <?php if($f['t_filename']== "" ){?>
                      <img class="profile-user-img img-fluid img-circle" src="images/images.png"
                       alt="User profile picture">
                  <?php }else{?>
                      <img class="profile-user-img img-fluid img-circle" src="images/<?php echo $f['t_filename'];?>"
                       alt="User profile picture">
                  <?php }?>
              
                </div>

                <h3 class="profile-username text-center"><?php echo $f['t_fname']." ".$f['t_lname'];?></h3>

                 <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b><i class="fa fa-map-marker mr-1"></i>Address</b> <a class="float-right"><?php echo $f['t_address'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b><i class="fa fa-envelope mr-1"></i>Email</b> <a class="float-right"><?php echo $f['t_email'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b><i class="fa fa-mobile mr-1"></i>Contact</b> <a class="float-right"><?php echo $f['t_contact'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b><i class="fa fa-book mr-1"></i>Education</b> <a class="float-right"> <?php echo $f['t_education'];?></a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills" id="tab">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Settings</a></li>
                  <li class="nav-item"><a class="nav-link" href="#up_image" data-toggle="tab">Update Image</a></li>
                  <li class="nav-item"><a class="nav-link" href="#student" data-toggle="tab">My Student </a></li>
                  <li class="nav-item"><a class="nav-link" href="#history" data-toggle="tab">My Advisory History</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <form class="form-horizontal" method="POST" id="update_profile">
                      <input type="hidden" class="form-control" id="t_id" name="t_id" value="<?php echo $f['t_id'];?>">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label" style="float: left;">First Name</label>

                        <div class="col-sm-10" style="float: left;">
                          <input type="text" class="form-control" id="inputName" name="fname" value="<?php echo $f['t_fname'];?>"  placeholder="First Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label" style="float: left;">Last Name</label>

                        <div class="col-sm-10" style="float: left;">
                          <input type="text" class="form-control" id="inputName" name="lname" value="<?php echo $f['t_lname'];?>" placeholder="Last Name">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label" style="float: left;">Address</label>

                        <div class="col-sm-10" style="float: left;">
                          <input type="text" class="form-control" id="inputName" name="address" value="<?php echo $f['t_address'];?>" placeholder="Last Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label" style="float: left;">Email</label>

                        <div class="col-sm-10" style="float: left;">
                          <input type="email" class="form-control" id="inputEmail" name="email" value="<?php echo $f['t_email'];?>"  placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 control-label" style="float: left;">Contact</label>
                        <div class="col-sm-10" style="float: left;">
                          <input type="text" class="form-control" id="inputName2" name="contact" 
                          value="<?php echo $f['t_contact'];?>" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 control-label" style="float: left;">Education</label>

                        <div class="col-sm-10" style="float: left;">
                          <input type="text" class="form-control" id="inputName2" name="education" 
                          value="<?php echo $f['t_education'];?>" placeholder="B.S. in Computer Science from the University of Tennessee at Knoxville">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10" style="float: left;">
                          <button type="submit" class="btn btn-success" id="btn_update">Submit</button>
                        </div>
                      </div>
                    </form>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="up_image">
                    <!-- Post -->
                   <form class="form-horizontal" method="POST" id="upload_image">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Upload Image</label>
                         <input type="hidden" class="form-control" id="t_id" name="t_id" value="<?php echo $f['t_id'];?>">
                        <div class="col-sm-10">
                          <input type="file" class="form-control" id="inputName" name="filename">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success" id="btn_upload_image">Submit</button>
                        </div>
                      </div>
                    </form>
                    <!-- /.post -->
                  </div>

                  <div class="tab-pane" id="student">
                     <div class="col-md-12 input-group" style="margin-bottom: 20px;">


                  <div class="input-group-prepend">
                    <span class="input-group-text"><strong> Year : <span style="color:red;">*</span></span></strong>
                  </div>
                    <select id="sy" style="text-transform: capitalize;margin-right: 10px;" onchange="getSection()">
                      <option value="">-- Select Year --</option>
                      <?php 

                      include "core/config.php";

                      $event = mysql_query("SELECT * from tbl_user where t_id='$t_id' group by year ");
                     while($row = mysql_fetch_array($event)){ ?>
                              <option value="<?php echo $row['year'];?>"><?php echo $row['year'] ?></option>

                      <?php } ?>
                    </select>

                    
                     <div class="input-group-prepend">
                      <span class="input-group-text"><strong> Section: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                   <select id="section" style="text-transform: capitalize;margin-right: 10px;">
                     <option value="">-- Select Year --</option>
                    </select>

                  <div class="col-md-3 input-group">
                    <button class="btn btn-primary btn-sm" onclick="gen()" id="btn_gen"><span class="fa fa-refresh"></span> Generate </button>
                  </div>
                  
                </div>
                  <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                      <thead>
                        <tr>
                          <th>Student ID</th>
                          <th>Name</th>
                          <th>Year Graduated</th>
                          <th>Section</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                     
                      </tbody>
                   
                    </table>
                    <!-- /.post -->
                  </div>

                    <div class="tab-pane" id="history">
                    <div class="row">
                      <button class="btn btn-primary btn-sm" onclick="showAddModal()" style="float: right;margin-bottom: 20px;"><span class="fa fa-plus-circle" > </span> Add Advisory </button>
                    </div>
                       <?php require 'modals/modal_add_history.php'; ?>
                      <?php require 'modals/modal_view_history.php'; ?>
                  <table id="tbl_history" class="table table-bordered table-striped" style="width: 100%;">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Year</th>
                        <th>Section</th>
                      </tr>
                      </thead>
                      <tbody>
                     
                     
                      </tbody>
                   
                    </table>
                    <!-- /.post -->
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include "footer.php";?>
<script type="text/javascript">



  $("#upload_image").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_teacher_image.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      if(data == 1){
              success_update();
              window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
            }else{
              window.location.reload();
            }
    }
  });
});

  function getSection(){
  var sy = $("#sy").val();
  var t_id = $("#t_id").val();
   $.post("ajax/getSection_teacher.php", {
                sy: sy,
                t_id:t_id
            },
            function (data) {
              $("#section").html(data);
     });
}

  $("#update_profile").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_teacher.php",
      type:"POST",
      data:$("#update_profile").serialize(),
      success: function(data){
          if(data == 1){
         success_update();
        }else{
          failed_query();
        }
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("Submit ");
      }
    });
  });

   function get_Student(){
  var sy = $("#sy").val();
  var section = $("#section").val();
  var t_id = $("#t_id").val();
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/advidor_student.php",
      "dataSrc":"data",
      "data":{
          t_id:t_id,
           sy: sy,
          section:section
        }
    },
    "columns":[
      {
        "data":"stud_id"
      },
      {
        "data":"name"
      },
      {
        "data":"year"
      },
      {
        "data":"section"
      }
    ]
  });
}
function get_ad(){

  var t_id = $("#t_id").val();
  var table = $('#tbl_history').DataTable();
  table.destroy();
  $("#tbl_history").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/get_my_advisory.php",
      "dataSrc":"data",
      "data":{
          t_id:t_id
        }
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"year"
      },  
      {
        "data":"section"
      }
    ]
  });
}

function  showAddModal(){
    var t_id = $("#t_id").val();
  $("#modalAddHistory").modal("show");
  $("#add_history_t_id").val(t_id);
}

function update(id,year,section){

        // $.post("ajax/get_ad_info.php", {
        //         id: id
        //     },
        //     function (data, status) {
        //         var o = JSON.parse(data);

          $("#view_year_history").val(year);
          $("#view_section_history").val(section);
          $("#view_id_history").val(id);
     // });
  $("#modalViewHistory").modal("show");
}
 $("#add_history").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_advisory.php",
      method:"POST",
      data:$("#add_history").serialize(),
      success: function(data){

        if(data == 1){
          get_ad();
         success_add();
         $("#add_history")[0].reset();
        }else if(data == 2){
          exist_year();
        }else{
         failed_query();
        }

        $("#modalAddHistory").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


 $("#update_history").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_history.php",
      method:"POST",
      data:$("#update_history").serialize(),
      success: function(data){

        if(data == 1){
          get_ad();
         success_add();
         $("#update_history")[0].reset();
        }else if(data == 2){
          exist_year();
        }else{
         failed_query();
        }
        $("#modalViewHistory").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });

 $("#up_image").click(function (){
    $("#up_image .tab-pane").addClass("active");

 });


  function gen() {
        var sy = $("#sy").val();
        var section = $("#section").val();

    if (sy == "" ||  section == ""){
      alert ("Please fill in the form");
    }else{
        $("#show").css("display","block");
        get_Student();
     } 
}
$(document).ready(function (){


  get_ad();
});
 </script>