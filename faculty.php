<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Faculty</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
           <div style="padding: 20px;padding-bottom: 0px;">
              
              <button class="btn btn-danger btn-sm pull-right" id="btn_delete" onclick="deleteFaculty()"><span class="fa fa-trash-o"> </span> Archive Faculty </button>
              <button class="btn btn-primary btn-sm pull-right" onclick="add_faculty()" style="margin-right: 10px;"><span class="fa fa-plus-circle"> </span> Add Faculty </button>
          </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dt_faculty" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><input type="checkbox" onchange="checkAll(this)"></th>
                  <th>#</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Year</th>
                  <th>Section</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
             
              </table>
               <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php require 'modals/modal_addFaculty.php'; ?>
  <?php require 'modals/modal_updateFaculty.php'; ?>
  <?php include "footer.php";?>

<script type="text/javascript">

function add_faculty(){
		$("#modalFaculty").modal('show');
}

$("#frm_addFaculty").submit(function(e){
	e.preventDefault();
		$("#btn_addFaculty").prop('disabled',true);
		$('#btn_addFaculty').html('<span class = "fa fa-spinner"></span> Loading ...')
	$.ajax({
		type:"POST",
		url:"ajax/add_faculty.php",
		data:$("#frm_addFaculty").serialize(),
		success:function(data){
			if(data == 1){
				success_add();
				get_details();
				$("#code").val("");
				$("#name").val("");
				$("#year").val("");
				$("#section").val("");
			}else{
				failed_query();
			}
				$("#modalFaculty").modal('hide');
				$("#btn_addFaculty").prop('disabled',false);
				$('#btn_addFaculty').html('<span class = "fa fa-check-circle"></span> Save')
			}
		});
	});

$("#frm_updateFaculty").submit(function (e){
	e.preventDefault();
		$("#btn_updateFaculty").prop('disabled',true);
		$('#btn_updateFaculty').html('<span class = "fa fa-spinner"></span> Save Changes ...')
	$.ajax({
		type:"POST",
		url:"ajax/update_faculty.php",
		data:$("#frm_updateFaculty").serialize(),
		success: function(data){
			if(data == 1){
				get_details();
				success_update();
			}else{
				failed_query();
			}

			$("#modal_updateFaculty").modal("hide");
		}
	});
});


  function deleteFaculty(){

  // <input type='' name='' class >
   var checkedValues =  $("input[name='delete_faculty']:checked").map(function(){
    return this.value;
  }).get();
    
  id = [];


  if(checkedValues == ""){
    alertMe("Aw Snap!","No selected Faculty","warning");
  }else{
    var retVal = confirm("Are you sure to delete?");
    if( retVal == true ){
      $("#btn_delete").prop("disabled", true);
      $("#btn_delete").html("<span class='fa fa-spin fa-spinner'></span> Loading");
      $.post("ajax/delete_faculty.php", {
        id: checkedValues
      },
      function (data, status) {
      $("#response").html(data);
      $("#modalResponse").modal('show');
        get_details();
      $("#btn_delete").prop("disabled", false);
      $("#btn_delete").html("<span class='fa fa-trash-o'></span> Delete Student");
      }

    );
    }

  }
  
}

function update_faculty(id){
$("#hidden_id").val(id);
$.post("ajax/getFaculty.php", {
	id:id
},
	function(data, status) {
		var o = JSON.parse(data);

		$("#up_code").val(o.code);
		$("#up_name").val(o.name);
		$("#up_year").val(o.year);
		$("#up_section").val(o.section);
	});
	 $("#modal_updateFaculty").modal("show");
	 	$("#btn_updateFaculty").prop('disabled',false);
		$('#btn_updateFaculty').html('<span class = "fa fa-check-circle"></span> Save')

}



function get_details(){
	$("#dt_faculty").DataTable().destroy();
	$('#dt_faculty').DataTable({
	"processing": true,
	"ajax":{
		"type":"POST",
		"url":"ajax/datatables/faculty_dt.php",
		"dataSrc":"data",
	},
	"columns":[
		{
			"mRender": function(data,type,row){
				return	"<input type='checkbox' value='"+row.id+"' name='delete_faculty'>";		
				}
		},
		{
			"data":"count"
		},
		{
			"data":"code"
		},
		{
			"data":"name"
		},
		{
			"data":"year"
		},
		{
			"data":"section"
		},
		{
			"mRender": function(data,type,row){
				return	"<center><button class='btn btn-info btn-sm' onclick='update_faculty("+row.id+")' data-toggle='tooltip' title='Update Record' value=' "+row.id+" '><span class='fa fa-pencil'></span> Update</button></center>";		
				}
		}
	]

	});	
}

$(document).ready(function (){
	get_details();
});
</script>