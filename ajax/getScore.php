<?php
include '../core/config.php';

if(isset($_POST["id"]) && isset($_POST["id"]) != ""){
	$audio_id = $_POST['id'];
	
	$query = "SELECT * from audio where audio_id = '$audio_id'";
	$result = $connectDB->query($query) or die(mysqli_error());
	$response = array();
	
	if(mysqli_num_rows($result) > 0){
		while ($row = mysqli_fetch_assoc($result)) {
			$quiz = mysqli_fetch_array($connectDB->query("SELECT * FROM quiz_header where quiz_id='$row[quiz_id]'"));
		 	$count_att = mysqli_fetch_array($connectDB->query("SELECT * FROM tbl_user where user_id='$quiz[user_id]'"));
		 	$subject = mysqli_fetch_array($connectDB->query("SELECT * FROM subjects where subject_id='$quiz[subject_id]'"));

			
            $response = $row;
            $response['quiz_name'] = $quiz['quiz_name'];
			$response['teacher'] = $count_att['fname']." ".$count_att['lname'];
			$response['level'] = $quiz['level'];
			$response['subject'] =$subject['subject_name'];
			$response['score'] =$row['score'];
			$response['date_added'] = date("F d, Y", strtotime($row['date_added']));
		}
	}else
    {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    echo json_encode($response);
}