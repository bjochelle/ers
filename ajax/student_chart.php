<?php

include '../core/config.php';
$id = $_GET['student_id'];

$f = $connectDB->query("SELECT * from subjects") or die(mysql_error());


date_default_timezone_set('Asia/Manila');
$date = date("Y-m-d");

$header['series'] = array();

$data['name'] = 'Score :';

$data['colorByPoint'] = true;
$data['data'] = array();

while ($row = mysqli_fetch_array($f)) {
		$list = array();
		
        $quiz = mysqli_fetch_array($connectDB->query("SELECT sum(a.score) as sc, count(*) as c FROM `audio` as a, quiz_header as h, subjects as s WHERE a.quiz_id = h.quiz_id AND h.subject_id = s.subject_id AND s.subject_id = '$row[subject_id]' AND stud_id = '$id'"));
        $count = ($quiz['c'] == 0)?1:$quiz['c'];
        $average = ($quiz['sc'] /  $count);

		if($average == 0 or $average == null or $average == ''){
			$total = 0;
		}else{	
			$total = ($average * 1);
		}
		$list['name'] = $row['subject_name'];
		$list['drilldown'] = $row['subject_name'];
		$list['y'] = $total;
		array_push($data['data'], $list);
	
}

$header['drilldown'] = array();

//for drilldown
$quizzes = $connectDB->query("SELECT * from quiz_header as h, subjects as s WHERE h.subject_id = h.subject_id") or die(mysql_error());
while($row_quiz = $quizzes->fetch_assoc()){
	
	$header_drilldown['series'] = array();



	$data_drilldown['name'] = $row_quiz['subject_name'];
	$data_drilldown['id'] = $row_quiz['subject_name'];
	$data_drilldown['data'] = array();
    $name = $row_quiz['quiz_name'];

	//for drilldown
	$get_score = $connectDB->query("SELECT * FROM audio as a, quiz_header as h where a.quiz_id = h.quiz_id AND a.quiz_id = '$row_quiz[quiz_id]' AND a.stud_id = '$id'");
    $fetch_score = $get_score->fetch_assoc();

	if($get_score->num_rows == 0){

		$list_drilldown = array();

		$list_drilldow = [$name,0];
		array_push($data_drilldown['data'], $list_drilldow);
	}else{
		while($score_row = mysqli_fetch_array($get_score)){
			$category = $score_row['quiz_name'];

			$list_drilldown = array();

			if($score_row['score'] == 0 or $score_row['score'] == null or $score_row['score']== ''){
				$total = 0;
			}else{	
				$total = ($score_row['score'] * 1);
			}
			$list_drilldow = [$category,$total];
			array_push($data_drilldown['data'], $list_drilldow);
		}
	}

array_push($header['drilldown'], $data_drilldown);
array_push($header_drilldown['series'], $data_drilldown);
}

array_push($header['series'], $data);
echo json_encode($header);
?>