<?php

include '../core/config.php';

$quiz_id = $_POST['quiz_id'];
$sy = $_POST['sy'];
$section = $_POST['section'];

if ($section == "-1"){
	$section_info = "All Section";
	$count_attend=mysqli_fetch_array($connectDB->query("SELECT count(*) FROM `audio` as a , tbl_student as s where a.stud_id=s.stud_id and year='$sy' and  quiz_id='$quiz_id'"));
}else{
	$section_info = "Section ".$section;
	$count_attend=mysqli_fetch_array($connectDB->query("SELECT count(*) FROM `audio` as a , tbl_student as s where a.stud_id=s.stud_id and year='$sy' and  quiz_id='$quiz_id' and section='$section'"));
}
			
$quiz_name = mysqli_fetch_array($connectDB->query("SELECT quiz_name from quiz_header where quiz_id='$quiz_id'"));

?> 

<div class="row">
		<div class="col-md-12" align="left" style="font-size:14pt; font-weight:bold;">
			<div class="col-md-2" style="float: left;"> 
				<img src="../images/logo_2.png" width="100px" > </img>
			</div>
			<div class="col-md-6" style="float: left;">
				<h5 style="margin:0px;"> Score Report </h5><h6 style=" text-transform: uppercase;"> <strong> Quiz Name : </strong> <?php echo $quiz_name[0]; ?></h6><h6>   <?php echo "Year ".$sy.", ".$section_info; ?></h6> <h6> <strong> Total # of Student take the Exam : </strong> <?php echo $count_attend[0]; ?> </h6>

			</div>
		</div>
	</div>
<table style="width: 100%;margin-top: 20PX;" class="table table-bordered table-striped">
	<thead>
		<tr style="background-color: #09509c  !important;color: white;font-weight: bold;">
		<td> # </td>
		<td> Name </td>
		<td> Score </td>
		<td> Date Taken</td>
		</tr>
	</thead>
	<tbody>
		<?php 

			if ($section=="-1"){
				$query=$connectDB->query("SELECT * FROM `audio` as a , tbl_student as s where a.stud_id=s.stud_id and year='$sy' and  quiz_id='$quiz_id' ");
			}else{
				$query=$connectDB->query("SELECT * FROM `audio` as a , tbl_student as s where a.stud_id=s.stud_id and year='$sy' and  quiz_id='$quiz_id' and section='$section'");
			}	
			
			
			$count=1;
			while( $row = mysqli_fetch_array($query)){

			$name = mysqli_fetch_array($connectDB->query("SELECT fname,lname from tbl_student where stud_id='$row[stud_id]'"));

		?>
		<tr >
			<td style="padding: 5px; font-size: 16px;"><?php echo $count++; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo $name[0]." ".$name[1]; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo $row['score']; ?></td>
			<td style="padding: 5px; font-size: 16px;"><?php echo date("F d, Y g:i a", strtotime($row['date_added'])); ?></td>
	
		</tr> 
	<?php }?>
	</tbody>
</table>