<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Reading Exercise</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
     <form id="add_quiz">
  <div class="form-row align-items-center">

    <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Reading Name</div>
        </div>
        <input type="text" class="form-control" id="inlineFormInputGroup" name="quiz_name" placeholder="Quiz Name">
      </div>
    </div>

    <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Subject</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="subject_id">
        <option selected>Choose...</option>
        <!-- Page specific script -->

        <?php 
        include "core/config.php";
        $checkNumEventsSQL = $connectDB->query("SELECT * FROM subjects");
         while($rowEvents = mysqli_fetch_array($checkNumEventsSQL)){
        ?> 
        <option value="<?=$rowEvents['subject_id']?>"><?=$rowEvents['subject_name']?> (<?=$rowEvents['subject_code']?>)</option>
          <?php }?>
      </select>
      </div>
    </div>

    <div class="col-sm-6" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Module</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="module_id">
        <option selected>Choose...</option>
        <!-- Page specific script -->

        <?php 
        include "core/config.php";
        $checkNumEventsSQL2 = $connectDB->query("SELECT * FROM modules");
         while($rowEvents2 = mysqli_fetch_array($checkNumEventsSQL2)){
        ?> 
        <option value="<?=$rowEvents2['module_id']?>"><?=$rowEvents2['module_name']?></option>
          <?php }?>
      </select>
      </div>
    </div>

     <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Level</div>
        </div>
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="level">
        <option selected>Choose...</option>
        <option value="easy">Easy</option>
        <option value="medium">Medium</option>
        <option value="hard">Hard</option>
      </select>
      </div>
    </div>
    <div class="col-sm-3" style="margin-top: 10px">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">Deadline</div>
        </div>
       <input type='date' name='deadline' class='form-control'>
      </div>
    </div>     

<div class="col-sm-12" style="margin-top: 10px">
    <textarea class="form-control" rows="10" cols="125" name="quiz_data"> Quiz info here</textarea>
 </div>

  </div>
      <div class="col-sm-12" style="margin-top: 10px">
      <button type="submit" class="btn btn-primary pull-right" id="btn_add">Create</button>
    </div>
</form>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    

  $("#add_quiz").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_quiz.php",
      method:"POST",
      data:$("#add_quiz").serialize(),
      success: function(data){
        if(data == 1){
         success_add();

        setTimeout(function(){
           window.location.href = "quiz.php";
         },1500)
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddFaculty").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });

</script>