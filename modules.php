<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">PHIL - IRI</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="padding: 20px;">
              <?php if($user_type === "T"){ ?>
              <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Create PHIL-IRI </button>
            <?php }?>
          </div>
          <?php require 'modals/modal_upload_faculty.php'; ?>
          <?php require 'modals/modal_addFaculty.php';?>
          <?php require 'modals/modal_upload_pdf.php';?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>PHIL-IRI Name</th>
                  <th>Subject</th>
                  <th>Level</th>
                  
                  <th>PHIL-IRI File</th>
                  <th>Created By</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    
$("#btn_download").click(function(e){
  e.preventDefault();
  var dl = confirm("Download Template for Teacher(csv file).");
  if(dl == true){
    window.location = 'ajax/download_template_teacher.php?bid=';
  }
  
});

    function showUploadModal(){
  $("#modalStudent").modal('show');
}

function showAddModal(){
  window.location.href = "createModule.php";
}

$("#upload_pdf").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url:"ajax/uploadPDFFile.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false, 
      cache: false,
      processData:false,
      success: function(data)
        {
          if (data == 1 ){
            success_add();
          }else{
            failed_query();
          }
          setTimeout(function(){
            window.location.reload();
          }, 1000); //refresh every 2 seconds
          $("#modalPDF").modal("hide");
        }
               
     });
  }));


$("#upload_faculty").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_template_faculty.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      $("#modalStudent").modal("hide");
      $("#modalResponse").modal('show');
      $("#response").html(data);
      
      get_Faculty();
    }
  });
});


  $("#add_faculty").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_faculty.php",
      method:"POST",
      data:$("#add_faculty").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_add();
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddFaculty").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_student").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_student.php",
      method:"POST",
      data:$("#update_student").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_update();
        }else if(data == 2){
          failed_query();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });

  function showModal(id){
    $("#modalPDF").modal();
    $("#moduleID").val(id);
  }

  function downloadFile(file){
    window.location.href = "ajax/uploads/"+file;
  }

  function get_Faculty(){
  var table = $('#example1').DataTable();
  table.destroy();

  var user_type = "<?php echo $user_type; ?>";
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/module_dt.php",
      "dataSrc":"data",
      "type":"POST",
      "data":{
          user_type:user_type,
        }
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"module_name"
      },
      {
        "data":"subject"
      },
      {
        "data":"level"
      },
      {
        "mRender": function(data,type,row){
          // var link = "//$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";?>";

          // alert(link);
          if(row.module === ''){
              if(row.roles == 1){
                var edit = "";
              }else{
                var edit = "No Files Attached";
              }
              
                var read = '';
          }else{
             var read = "<a href='#' onclick='downloadFile(\""+row.module+"\")'>"+row.module+" </a>";
              var edit = '';
          }
          return "<center>"+ edit + read+ " </center>";
        }
      },
      {
        "data":"teacher"
      },
      {
        "mRender": function(data,type,row){

          if(user_type === 'T'){
              var edit = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Record' value='" + row.id+ "' onclick='window.open(\"editModule.php?id=" + row.id +"\")'><span class='fa fa-pencil'></span> Edit</button>";
                var read = '';
                 var file = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Upload/Update module file' onclick='showModal("+row.id+")'><span class='fa fa-upload'></span> Upload</button>";
          }else  if(user_type === 'A'){
              var read = '';
              var edit = '';
               var file = "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Upload/Update module file' onclick='showModal("+row.id+")'><span class='fa fa-upload'></span> Upload</button>";

          }else{
             var read = "<button class='btn btn-success btn-sm' data-toggle='tooltip' title='Read Module' value='" + row.id+ "' onclick='window.open(\"readModule.php?id=" + row.id +"\")'><span class='fa fa-eye'></span> Read </button>";
              var edit = '';
               var file = '';
          }

         
          return "<div style='display: flex;flex-wrap: nowrap;align-content: center;justify-content: center;align-items: center;'>"+ edit + read+ file +"</div>";
        }
      }
    ]
  });
}
  
$(document).ready(function (){
  get_Faculty();
});
</script>