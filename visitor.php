<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Visitor </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Time In</th>
                  <th>Time Out</th>
                  <th>Purpose</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
             
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php include "footer.php";?>
  <script type="text/javascript">

    function get_visitor(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/visitor_dt.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"visitor_name"
      },
      {
        "data":"time_in"
      },
      {
        "data":"time_out"
      },
      {
        "data":"purpose"
      }
    ]
  });
}
  
$(document).ready(function (){
  get_visitor();
});
  </script>