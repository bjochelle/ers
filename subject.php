<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Subject</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="padding: 20px;">
            
              <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Add Subject </button>
          </div>
          <?php require 'modals/modal_update_subject.php'; ?>
          <?php require 'modals/modal_add_subject.php';?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Subject Code</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    

function showAddModal(){
  $("#modalAddSubject").modal('show');
}

function update(id){
        $.post("ajax/getSubject.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
          $("#subject_code").val(o.subject_code);
          $("#subject_name").val(o.subject_name);
          $("#subject_id").val(o.subject_id);
     });
  $("#modalUpdateStudent").modal("show");
}

  $("#add_subject").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_subject.php",
      method:"POST",
      data:$("#add_subject").serialize(),
      success: function(data){
        if(data == 1){
          get_Subject();
         success_add();
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddSubject").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_subject").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_subject.php",
      method:"POST",
      data:$("#update_subject").serialize(),
      success: function(data){
        if(data == 1){
          get_Subject();
         success_update();
        }else if(data == 2){
          failed_query();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });



  function get_Subject(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/subject_dt.php",
      "dataSrc":"data"
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"sub_code"
      },
      {
        "data":"sub_desc"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Update Record' value='" + row.id+ "' onclick='update("+row.id+")'><span class='fa fa-pencil'></span> Edit </button></center>";
        }
      }
    ]
  });
}
  
$(document).ready(function (){
  get_Subject();
});
</script>