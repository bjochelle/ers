﻿ <!DOCTYPE HTML>
<html>

<head>
	  <title>MANLUCAHOC ELEMENTARY SCHOOL</title>

  <link rel="icon" type="image/ico" href="images/images.jpg" />
	<!-- Meta-Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" >
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //Meta-Tags -->
	<!-- Stylesheets -->
	<link href="plugins/css/font-awesome.css" rel="stylesheet">
	<link href="plugins/css/style.css" rel='stylesheet' type='text/css' />
	<link href="dist/css/animate.css" rel='stylesheet' type='text/css'/>
	<!--// Stylesheets -->
	<!--fonts-->
	<!--//fonts-->

	<style type="text/css">
		.w3ls-login label i{
			    color: #0f7b0a !important;
		}
		.copy-wthree p{
			color: #0f7b0a !important;
		}
	</style>
</head>

<body >
	<div class="w3ls-login col-md-12" >
		<!-- form starts here -->

			
		<form action="#" method="post" id="login" style="max-width: 100% !important;margin-top: 10%;"	>
			
			<div class="col-md-6" style="margin-right: 20px;">
				
				<img src="images/logo_2.png" width="200px" > </img>
			</div>

			<div class="col-md-6">
				<h2 style="color:#0f7b0a  !important;    font-size: 33px;
    color: #0f7b0a !important;
    font-weight: bold;
    margin-bottom: 10px;"> Login Form </h2>
				<div class="agile-field-txt">
				<label> Username :</label>
				<input type="text" name="username" placeholder=" " required="" autocomplete="off" />
			</div>
			<div class="agile-field-txt" style="">
				<label>password :</label>
				<input type="password" name="password" placeholder=" " required="" id="myInput" />
			</div>
			
			<!-- //script for show password -->
			<div class="w3ls-login  w3l-sub">
				<input type="submit" value="Login" id="btn_login" style="background: #0f7b0a ;">
			</div>
		
			<div class="w3ls-login  w3l-sub animated " id="notif" style="color: #e91e63;"> </div>

			</div>
			
		</form>
	</div>



<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript">

		
	$("#login").submit(function(e){
		$("#notif").removeClass("shake");
	  e.preventDefault();
	  $.ajax({
	    url:"ajax/login_user.php",
	    method:"POST",
	    data:$("#login").serialize(),
	    success: function(data){
	      if(data == 1){
	      	window.location.replace("dashboard.php");
	      }else{
	      	$("#notif").html("Incorrect Username or Password");
	      	$("#notif").addClass("shake");
	      }
	    }
	  });
	});

	</script>
</body>

</html>