<?php include "header.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Archiving </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No. </th>
                  <th>Name</th>
                  <th>User Type</th>
                  <th>Date Archive</th>
		   <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
             
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php include "footer.php";?>
  <script type="text/javascript">

    function get_arch(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/archive_dt.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"type"
      },
      {
        "data":"date"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Restore Record' id='" + row.id+ "' onclick='update(" + row.id+")'><span class='fa fa-pencil'></span> Restore </button></center>";
        }
      }
    ]
  });
}



  function update(id){

    $("#"+id).prop("disabled",true);
    $("#"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    $.ajax({
      url:"ajax/update_archive.php",
      method:"POST",
      data:{
	id:id
	},
      success: function(data){
          if(data == 1){
          get_arch();
         success_update();
        }else{
          failed_query();
        }
      }
    });
  }



  
$(document).ready(function (){
  get_arch();
});
  </script>